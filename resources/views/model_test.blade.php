@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
  <?php
        if(isset(Auth::user()->id)){ ?>
           <input type="hidden" name="user_id" id="userId" value="<?php echo Auth::user()->id;?>">
        <?php } ?>
  <p id="demo"></p>
  <form class="form-horizontal" name="add_product" action="{{url('/model-test-set')}}" method="get" enctype="multipart/form-data">
	{{ csrf_field() }}
    <div class="col-md-8 col-md-offset-2">
	 <div class="panel panel-default">
	  <div class="panel-heading">Model Test</div>
		<section class="content">
			<!-- SELECT2 EXAMPLE -->
			<div class="panel-body">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
				     <div class="col-md-12">
						<div class="form-group">
							<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Target Category</label>
							<div class="col-md-6">
								<select name="target" class="form-control" id="target_id">
										<option value=""> --- Select Target ---</option>
									<?php foreach($target_list as $target){ ?>
										<option value="<?php echo $target->id;?>" @if(isset($target_cat) && ($target->id == $target_cat)) selected @endif><?php echo $target->target_name;?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Category</label>
							<div class="col-md-6">
									<select name="category_id" value="{{ $cat or '' }}" class="form-control" id="category_id">
                                      <option value="{{ $cat }}"> --- Select Category ---</option>
									</select>
							</div>
						</div>
						<div class="form-group">
							<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Subject</label>
							<div class="col-md-6">
								<select name="subject_id" class="form-control" id="subject_id" required>
										<option value=""> --- Select Subject ---</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Set Code</label>
							<div class="col-md-6">
								<select name="set_code" class="form-control" id="set_code" required>
										<option value=""> --- Select Set Code ---</option>
								</select>
							</div>
						</div>
						<div class="box-footer">
							<button type="submit" name="btnsubmit" class="submitbtn btn btn-primary pull-right">Submit</button>
						</div>
						<!-- <?php 
							$i=1;
							foreach($iqinfos as $iqinfo){
		   						echo "<h3>".$iqinfo->question_text."</h3></br>";
								echo 
								'<input type="radio" name="gender_'.$i.'"  value="'.$iqinfo->option_one.'">'.$iqinfo->option_one.'
								<input type="radio" name="gender_'.$i.'" value="'.$iqinfo->option_two.'">  '.$iqinfo->option_two.'
								<input type="radio" name="gender_'.$i.'" value="'.$iqinfo->option_three.'">  '.$iqinfo->option_three.'
								<input type="radio" name="gender_'.$i.'" value="'.$iqinfo->option_four.'">  '.$iqinfo->option_four.
								$i++;
							}
							?>							 -->
						</div>
						<hr>
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<button type="submit" name="btnsubmit" class="submitbtn btn btn-primary pull-right">Submit Answer</button>
				</div>
			  </div>
		    </section>
	      </form>
		</div>
	</div>
   </div>
</div>
<script>
$( document ).ready(function() {
	$("#target_id").on('change',function(){
		var target_id=$("#target_id").val();
		var url=base_url + "/get-categoryby-target/"+target_id;
		$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: '',
				success: function (data)
				{
					$('#category_id').empty();
					$('#category_id').append('<option value=""> -- Select Category -- </option>');
					$.each(data, function (index, cat_obj) {
						$('#category_id').append('<option value="' + cat_obj.id + '">' + cat_obj.category_name + '</option>');
					});
					$("#category_id").on('change',function(){
						var category_id=$("#category_id").val();
		                var url=base_url + "/get-subjectby-category/"+category_id;
						$.ajax({
                             url:url,
							 type:'GET',
							 datatype:'json',
							 data: '',
							 success:function(data){
								 $("#subject_id").empty();
								 $("#subject_id").append("<option value=''> -- Select Subject -- </option>");
								 var subj_list=jQuery.parseJSON(data);
                                 $.each(subj_list, function (index, subject_obj) {
									$('#subject_id').append('<option value="' + subject_obj.id + '">' + subject_obj.subject_name + '</option>');
								 });
								$("#subject_id").on('change',function(){
									var subject_id=$("#subject_id").val();
									var url=base_url + "/get-subjectwise-set/"+subject_id;
									$.ajax({
										url:url,
										type:'GET',
										datatype:'json',
										data:'',
										success:function(data){
                                          //  alert(data);
										$("#set_code").empty();
										$("#set_code").append("<option value=''> -- Select Set Code -- </option>");
										var subj_list=jQuery.parseJSON(data);
										$.each(subj_list, function (index, subject_obj) {
											$('#set_code').append('<option value="' + subject_obj.set_code + '">' + subject_obj.set_code + '</option>');
										});

										$("#set_code").on('change',function(){
											var target_id=$("#target_id").val();
											var category_id=$("#category_id").val();
											var subject_id=$("#subject_id").val();
											var set_code = $(this).val();
											var ans_set_check_url=base_url + "/answer_set_check/"+target_id + '/'+ category_id + '/' + subject_id + '/' + set_code; 
											$.ajax({
												url:ans_set_check_url,
												type:'GET',
												datatype:'json',
												data: '',
												success:function(data){
                                                  if(data>=1){
														alert("You have already answer this set of questions");
														$('#set_code').val('');
												  }else{

												  }
												}
											});

										});

										}
									});
								});
							 }
						});
					});
				}
			});
	});
	function checkDelete() {
		var checkstr = confirm('Are you sure you want to delete this?');
		if (checkstr == true) {
			return true;
		} else {
			return false;
		}
	}
});
</script>
  <script src="{{ asset('assets/js/point.js') }}"></script>
@endsection