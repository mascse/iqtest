<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'MCQ') }}</title>
    <!-- Styles -->
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
	<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
    <script src="{{asset('assets/jQuery/jquery-3.1.1.min.js')}}"></script>
    <script>
            var base_url = "{{ URL::to('') }}";
            var csrf_token = "{{ csrf_token() }}";
    </script>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'MCQ') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar
                    <ul class="nav navbar-nav">
                        �
                    </ul>
                    -->
                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
						    <li><a href="{{ route('home') }}">Home</a></li>
						    <li><a href="{{ route('home') }}">Reading</a></li>
							<li><a href="{{ route('select.exam') }}">Select Exam</a></li>
							<!---<li><a href="{{ route('add.question') }}">Add Question</a></li> --->
							<li><a href="{{ route('model.test') }}">Model Test</a></li>
							<li><a href="#">Notice</a></li>
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
						<li><a href="{{ route('home') }}">Home</a></li>
						<li><a href="{{ route('home') }}">Reading</a></li>
						<li><a href="{{ route('select.exam') }}">Select Exam</a></li>
						<li><a href="{{ route('model.test') }}">Model Test</a></li>
						<li><a href="#">Notice</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('user.logout') }}">
                                            Logout
                                        </a>
                                        <!--<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form> --->
                                    </li>
                                </ul>
                            </li>
							
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
        @yield('content')
    </div>
    <div class="col-md-12" style="background-color:white;padding: 10px;text-align: center; border: 1px solid #ccc9c9ee;">
      <div class="">
        <div id="">© Copyright 2019 <a href="www.examtestbd.com">www.examtestbd.com</a></div>
      </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('assets/js/app.js') }}"></script>
	<script src="{{asset('assets/add_product.js')}}"></script>
	<link rel="stylesheet" href="{{asset('assets/previewForm.css')}}">
	<script src="{{asset('assets/previewForm.js')}}"></script>
	<script type="text/javascript" src="{{asset('assets/dynamic_select_box.js')}}"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
	@toastr_js
    @toastr_render
	<script>
		$(document).ready(function () {
			$('#myform').previewForm();
		});
	</script>
</body>
</html>
