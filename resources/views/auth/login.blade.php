@extends('layouts.app')
@section('content')
<link href="{{ asset('assets/input_style.css') }}" rel="stylesheet">
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                  @if (\Session::has('failed'))
                        <div class="">
                            <ul class="list-group">
                                <li class="text-center list-group-item list-group-item-{!! \Session::get('class') !!}" style="color:red;">{!! \Session::get('failed') !!}</li>
                            </ul>
                        </div>
                    @endif
                    <form class="form-horizontal" method="POST" action="{{ route('user.login') }}">
                        {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>
                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
					<div class="col-md-8 col-md-offset-2">
					<div class="omniauth-container prepend-top-15">
						<label class="label-bold d-block">
							Sign in with
						</label>
						<div class="d-flex justify-content-between flex-wrap">
                        <a class="btn d-flex align-items-center omniauth-btn text-left oauth-login qa-saml-login-button" id="oauth-login-github" rel="nofollow" data-method="post" href="{{url('/auth/facebook')}}">
								<img alt="GitHub" title="Sign in with GitHub" class="js-lazy-loaded" src="{{url('/')}}/storage/app/public/social/1000px-F_icon.svg_-1.png">
								<span>
									Facebook
								</span>
							</a>
                        <a class="btn d-flex align-items-center omniauth-btn text-left oauth-login qa-saml-login-button" id="oauth-login-google_oauth2" rel="nofollow" data-method="post" href="{{url('/auth/google')}}">
								<img alt="Google" title="Sign in with Google" class="js-lazy-loaded" src="{{url('/')}}/storage/app/public/social/new-google-favicon-512.png">
								<span>
									Google
								</span>
							</a>
							<!--<a class="btn d-flex align-items-center omniauth-btn text-left oauth-login qa-saml-login-button" id="oauth-login-twitter" rel="nofollow" data-method="post" href="#">
							<img alt="Twitter" title="Sign in with Twitter" class="js-lazy-loaded" src="{{url('/')}}/storage/app/public/social/twitter.png">
								<span>
								Twitter
								</span>
							</a> --->
						</div>
						<!--<fieldset class="remember-me">
							<label>
								<input type="checkbox" name="remember_me" id="remember_me" class="remember-me-checkbox">
								<span>
									Remember me
								</span>
							</label>
						</fieldset> --->
				      </div>
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
