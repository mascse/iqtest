@extends('admin.layouts.app')
@section('title', 'Category Management')
@section('content')

<!--<link rel="stylesheet" href="{{asset('assets_admin/dist/css/form_design.css')}}">-->
<section class="content-header">
    <h1>
        Category
        <small>Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Category</a></li>
        <li class="active">Management</li>
    </ol>
</section>
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Category Management</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
             <div>
                <a class="btn btn-sm btn-info btn-fla showadd_form" href="#">Add Category</a>
            </div>
            <div class="row">
                <div class="box-body">
                                <center>
                                    @if (session('save'))
                                    <div class="alert alert-success">
                                        {{ session('save') }}
                                    </div>
                                    @endif
                                </center>
                                <center>
                                    @if (session('error'))
                                    <div class="alert alert-success">
                                        {{ session('error') }}
                                    </div>
                                    @endif
                                </center>
                    <div class="row">
                        <div class="col-md-1"></div>
                          <div id="add-form" style="display:none;">
                        <form action="{{route('save.category')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-md-4">
                            <div class="form-group {{ $errors->has('target_id') ? ' has-error' : '' }}">
                                    <label>Target Category</label>
                                    <select name="target_id" class="form-control">
                                        <option value=""> --- Select Target ---</option>
                                        <?php foreach($target_list as $target){ ?>
                                            <option value="<?php echo $target->id;?>"><?php echo $target->target_name;?></option>
                                        <?php } ?>
                                    </select>
                                    @if ($errors->has('target_id'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('target_id') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('category_name') ? ' has-error' : '' }}">
                                    <label>Category Name</label>
                                    <input type="text" class="form-control" id="required-input" name="category_name" id="txtstyleref" required>
                                    @if ($errors->has('category_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('category_name') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Add"/>
                                </div>
                            </div>
                            <div class="col-md-7">

                            </div>
                        </form>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Browse All Category</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th width="5%" style="text-align:center;">SL</th>
                                    <th>Target Category</th> 
                                    <th>Category Name</th> 
                                    <th style="text-align:center;"> 
                                        Action
                                    </th>                                    
                                </tr>
                            </thead>
                            <tbody>
                             <?php $i=0; foreach($category_list as $target) { $i++;?>
                                 <tr>
                                     <td><?php echo $i;?></td>
                                     <td><?php echo $target->target_name;?></td>
                                     <td><?php echo $target->category_name;?></td>
                                     <td style="text-align:center;">
                                        <a href='{{url("/edit-category/{$target->id}")}}' class="btn btn-success btn-sm"><i class="fa fa-edit (alias)"></i></a>
                                        <a  onclick="return checkDelete()" href='{{url("/delete-target/{$target->id}")}}' class="btn btn-danger btn-sm"><i class="fa fa-close (alias)"></i></a>
                                    </td>
                                 </tr>
                              <?php  } ?>
                            </tbody>
                        </table>
                        <div class="row">
                        </div>
                        <!-- /.table-responsive -->
                    </div>
                </div>

            </div>
        </div>

    </div>

</div>
</section>
<style>
    .textRight{
        text-align:right;
    }
</style>
<script>
  $(".showadd_form").click(function () {
            $('#add-form').toggle(600);
            $(this).toggleClass('active');

        });
    function checkDelete() {
        var checkstr = confirm('Are you sure you want to delete this?');
        if (checkstr == true) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endsection


