@extends('admin.layouts.app')
@section('title', 'Subject Management')
@section('content')

<!--<link rel="stylesheet" href="{{asset('assets_admin/dist/css/form_design.css')}}">-->
<section class="content-header">
    <h1>
            Subject
        <small>Management</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Subject</a></li>
        <li class="active">Management</li>
    </ol>
</section>
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Subject Management</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="box-body">
                                <center>
                                    @if (session('save'))
                                    <div class="alert alert-success">
                                        {{ session('save') }}
                                    </div>
                                    @endif
                                </center>
                                <center>
                                    @if (session('error'))
                                    <div class="alert alert-success">
                                        {{ session('error') }}
                                    </div>
                                    @endif
                                </center>
                    <div class="row">
                        <div class="col-md-1"></div>
                          <div id="add-form" >
                        <form action="{{route('update.subject')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-md-4">
                              <div class="form-group {{ $errors->has('target_id') ? ' has-error' : '' }}">
                                    <label>Target Category</label>
                                    <select name="target_id" class="form-control" id="target_id">
                                        <option value=""> --- Select Target ---</option>
                                        <?php foreach($target_list as $target){ ?>
                                            <option value="<?php echo $target->id;?>"><?php echo $target->target_name;?></option>
                                        <?php } ?>
                                    </select>
                                    @if ($errors->has('target_id'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('target_id') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <div class="form-group {{ $errors->has('category_id') ? ' has-error' : '' }}">
                                        <label>Select Category</label>
                                        <select name="category_id" class="form-control" id="category_id">
                                            <option value=""> --- Select Category ---</option>
                                            <?php foreach($category_list as $category){ ?>
                                                <option value="<?php echo $category->id;?>"><?php echo $category->category_name;?></option>
                                            <?php } ?>
                                        </select>
                                        @if ($errors->has('category_id'))
                                          <span class="help-block">
                                              <strong>{{ $errors->first('category_id') }}</strong>
                                          </span>
                                        @endif
                                    </div>
                                <div class="form-group {{ $errors->has('subject_name') ? ' has-error' : '' }}">
                                    <label>Subject Name</label>
                                <input type="hidden" class="form-control" id="required-input" name="id" value="{{$subject_list->id}}" id="txtstyleref" required>
                                <input type="text" class="form-control" id="required-input" name="subject_name" value="{{$subject_list->subject_name}}" id="txtstyleref" required>
                                    @if ($errors->has('subject_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('subject_name') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Update"/>
                                </div>
                            </div>
                            <div class="col-md-7">

                            </div>
                        </form>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<style>
    .textRight{
        text-align:right;
    }
</style>
<script>
        document.getElementById("target_id").value = "<?php echo $subject_list->target_id; ?>";
        document.getElementById("category_id").value = "<?php echo $subject_list->category_id; ?>";
</script>
<script>
  $(".showadd_form").click(function () {
            $('#add-form').toggle(600);
            $(this).toggleClass('active');

        });
   $("#target_id").on('change',function(){
       var target_id=$("#target_id").val();
       var url=base_url + "/get-categoryby-target/"+target_id;
       $.ajax({
                url: url,
                type: 'GET',
                dataType: 'json',
                data: '',
                success: function (data)
                {
                    //   alert(data);
                    $('#category_id').empty();
                    $('#category_id').append('<option value=""> -- Select Category -- </option>');
                    $.each(data, function (index, subcatobj) {
                        $('#category_id').append('<option value="' + subcatobj.id + '">' + subcatobj.category_name + '</option>');
                    });
                }
            });
   });
    function checkDelete() {
        var checkstr = confirm('Are you sure you want to delete this?');
        if (checkstr == true) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endsection


