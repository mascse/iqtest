@extends('admin.layouts.app')
@section('title', 'Target Edit')
@section('content')

<!--<link rel="stylesheet" href="{{asset('assets_admin/dist/css/form_design.css')}}">-->
<section class="content-header">
    <h1>
        Target
        <small>Edit</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Target</a></li>
        <li class="active">Edit</li>
    </ol>
</section>
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Edit Target</h3>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="box-body">
                                <center>
                                    @if (session('save'))
                                    <div class="alert alert-success">
                                        {{ session('save') }}
                                    </div>
                                    @endif
                                </center>
                                <center>
                                    @if (session('error'))
                                    <div class="alert alert-success">
                                        {{ session('error') }}
                                    </div>
                                    @endif
                                </center>
                    <div class="row">
                        <div class="col-md-1"></div>
                          <div id="add-form">
                        <form action="{{route('update.target')}}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="col-md-4">
                                <div class="form-group {{ $errors->has('target_name') ? ' has-error' : '' }}">
                                    <label>Target Name</label>
                                <input type="hidden" class="form-control"  name="id" value="{{$target->id}}" id="txtstyleref" required>
                                <input type="text" class="form-control" id="required-input" name="target_name" value="{{$target->target_name}}" id="txtstyleref" required>
                                    @if ($errors->has('target_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('target_name') }}</strong>
                                    </span>
                                @endif
                                </div>
                                <div class="form-group">
                                    <input type="submit" name="btnsubmit" class="btn bg-navy btn-flat margin" value="Update"/>
                                </div>
                            </div>
                            <div class="col-md-7">

                            </div>
                        </form>
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<style>
    .textRight{
        text-align:right;
    }
</style>
<script>
  $(".showadd_form").click(function () {
            $('#add-form').toggle(600);
            $(this).toggleClass('active');

        });
    function checkDelete() {
        var checkstr = confirm('Are you sure you want to delete this?');
        if (checkstr == true) {
            return true;
        } else {
            return false;
        }
    }
</script>
@endsection


