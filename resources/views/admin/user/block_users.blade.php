@extends('admin.layouts.app')
@section('title', 'Manage User')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <h1>
          User
          <small>Manage</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li>User</li>
          <li class="active">Manage</li>
        </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
	 <div class="panel panel-default">
	  <div class="panel-heading">Manage User</div>
		<section class="content">
			<!-- SELECT2 EXAMPLE -->
			<div class="panel-body">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
                        <center>
                                @if (session('save'))
                                <div class="alert alert-success">
                                    {{ session('save') }}
                                </div>
                                @endif
                            </center>
                            <center>
                                @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                                @endif
                            </center>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align:center;">SL</th>
                                        <th>Full Name</th>
                                        <th>Email</th> 
                                        <th>Phone</th> 
                                        <th>Country</th> 
                                        <th>Social</th> 
                                        <th>Avatar</th> 
                                        <th style="text-align:center;">Block Status</th>
                                        <th style="text-align:center;"> 
                                            Action
                                        </th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=0;  foreach($user_list as $user){ $i++; ?>
                                     <tr>
                                        <td><?php echo $i;?></td>
                                        <td><?php echo $user->name;?></td>
                                        <td><?php echo $user->email;?></td>
                                        <td><?php echo $user->phone_number;?></td>
                                        <td><?php echo $user->country;?></td>
                                        <td><?php echo $user->service;?></td>
                                        <td><img class="img-responsive" src="<?php echo $user->avatar;?>" height="50" width="50"/></td>
                                        <?php if($user->block_status == 0){ ?>
                                        <td style="text-align:center;">
                                            <a href='{{url("/admin/user-block/{$user->id}")}}' class="btn btn-success btn-sm" title="Block"> <i class="fa fa-arrow-circle-down"></i></a>
                                        </td>
                                       <?php    }else{ ?>
                                        <td style="text-align:center;">
                                                <a href='{{url("/admin/user-unblock/{$user->id}")}}' class="btn btn-danger btn-sm" title="Unblock"> <i class="fa fa-arrow-circle-up"></i></a>
                                        </td>
                                        <?php   } ?>
                                        <td style="text-align:center;">
                                           <!--<a href='{{url("/admin/edit-question/{$user->id}")}}' class="btn btn-success btn-sm"> <i class="fa fa-edit (alias)"></i></a> -->
                                           <a  onclick="return checkDelete()" href='{{url("/admin/user-delete/{$user->id}")}}' class="btn btn-danger btn-sm"><i class="fa fa-close (alias)"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
						</div>
						<hr>
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					
				</div>
			  </div>
		    </section>
	      </form>
		</div>
	</div>
   </div>
</section>
<script>
	function checkDelete() {
		var checkstr = confirm('Are you sure you want to delete this user?');
		if (checkstr == true) {
			return true;
		} else {
			return false;
		}
	}
</script>
@endsection