<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title')</title>
        <link rel="icon" type="image/png" href="{{asset('asset_admin/images/uticon.ico')}}">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="{{asset('asset_admin/bootstrap/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{asset('asset_admin/font-awesome/css/font-awesome.min.css')}}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{asset('asset_admin/dist/css/ionicons.min.css')}}">

        <link rel="stylesheet" href="{{asset('asset_admin/plugins/iCheck/all.css')}}">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="{{asset('asset_admin/plugins/colorpicker/bootstrap-colorpicker.min.css')}}">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="{{asset('asset_admin/plugins/timepicker/bootstrap-timepicker.min.css')}}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{asset('asset_admin/plugins/select2/select2.min.css')}}">
        <link rel="stylesheet" href="{{asset('asset_admin/plugins/datatables/dataTables.bootstrap.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('asset_admin/dist/css/fileinput.min.css')}}">
        <link rel="stylesheet" href="{{asset('asset_admin/dist/css/AdminLTE.min.css')}}">
        <link rel="stylesheet" href="{{asset('asset_admin/dist/css/fileinput.min.css')}}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{asset('asset_admin/dist/css/skins/_all-skins.min.css')}}">
        <link rel="stylesheet" href="{{asset('asset_admin/plugins/morris/morris.css')}}">
        <link rel="stylesheet" href="{{asset('asset_admin/plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
        <!-- daterange picker -->
        <link rel="stylesheet" href="{{asset('asset_admin/plugins/daterangepicker/daterangepicker.css')}}">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="{{asset('asset_admin/plugins/datepicker/datepicker3.css')}}">
        <link rel="stylesheet" href="{{asset('asset_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">       
        <script src="{{asset('asset_admin/plugins/jQuery/jquery-3.1.1.min.js')}}"></script>

        <!-- iCheck for checkboxes and radio inputs -->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <script>
           var base_url = "{{ URL::to('') }}";
           var csrf_token = "{{ csrf_token() }}";
        </script>
        <style>
            .bv-form .help-block {
                margin-bottom: 0;
            }
            .bv-form .tooltip-inner {
                text-align: left;
            }
            .nav-tabs li.bv-tab-success > a {
                color: #3c763d;
            }
            .nav-tabs li.bv-tab-error > a {
                color: #a94442;
            }

            .bv-form .bv-icon-no-label {
                top: 0;
            }

            .bv-form .bv-icon-input-group {
                top: 0;
                z-index: 100;
            }
            .error{
                color: red;
                padding: 5px;
            }
        </style>
    </head>
	<?php
  //  $permission = DashboardController::getRolePermission();
    ?>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="{{url('/admin')}}" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>U</b>T</span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Exam</b> Test BD</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Notifications: style can be found in dropdown.less -->
                            <li class="dropdown notifications-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-bell-o"></i>
                                    <span class="label label-warning">10</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 10 notifications</li>
                                    <li>
                                        <!-- inner menu: contains the actual data -->
                                        <ul class="menu">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="footer"><a href="#">View all</a></li>
                                </ul>
                            </li>
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="{{url('/')}}/storage/app/public/user_image/{{ Auth::user()->image }}" class="user-image" alt="User Image">
                                    <span class="hidden-xs">{{ Auth::user()->name }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="{{url('/')}}/storage/app/public/user_image/{{ Auth::user()->image }}" class="img-circle" alt="User Image">

                                        <p>
                                            {{ Auth::user()->name }}
                                            <small>since Nov. 2018</small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="#" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{ route('admin.logout') }}" class="btn btn-default btn-flat">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{url('/')}}/storage/app/public/user_image/{{ Auth::user()->image }}" class="img-circle" alt="User Image">
                        </div>
                        <div class="pull-left info">
                            <p>{{ Auth::user()->name }}</p>
                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <form action="#" method="get" class="sidebar-form">
                        <div class="input-group">
                            <input type="text" name="q" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
                                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                                </button>
                            </span>
                        </div>
                    </form>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="{{url('/home')}}"><i class="fa fa-circle-o"></i> Dashboard</a></li>
          </ul>
        </li>
        <li class="treeview">
            <a href="#">
              <i class="fa fa-pie-chart"></i>
              <span>Category</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
               <li><a href="{{route('add.target')}}"><i class="fa fa-circle-o"></i> Manage Target</a></li>
               <li><a href="{{route('add.category')}}"><i class="fa fa-circle-o"></i> Manage Category</a></li>
               <li><a href="{{route('add.subject')}}"><i class="fa fa-circle-o"></i> Manage Subject</a></li>
            </ul>
          </li>
          <li class="treeview">
                <a href="#">
                  <i class="fa fa-question"></i>
                  <span>Question</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                   <li><a href="{{route('admin.add.question')}}"><i class="fa fa-circle-o"></i> Add Question</a></li>
                   <li><a href="{{route('admin.manage.question')}}"><i class="fa fa-circle-o"></i> Manage Question</a></li>
                </ul>
              </li>
              <li class="treeview">
                    <a href="#">
                      <i class="fa  fa-user"></i>
                      <span>Member</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu">
                    <li><a href="{{route('admin.payment_status')}}"><i class="fa fa-circle-o"></i> Payment Stauts</a></li>
                      <li><a href="{{route('admin.manage.user')}}"><i class="fa fa-circle-o"></i> Total Member</a></li>
                      <li><a href="{{route('admin.manage.user.block')}}"><i class="fa fa-circle-o"></i> Block Member</a></li>
                      <li><a href="#"><i class="fa fa-circle-o"></i> Account Total</a></li>
                    </ul>
              </li>
        <!-- user role ---
		<li class="treeview">
			<a href="#">
				<i class="fa fa-user"></i> <span>User</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li class="treeview">
					<a href="#"><i class="fa fa-circle-o"></i> Manage User
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{url('/add-user')}}"><i class="fa fa-circle-o"></i> Add User</a></li>
						<li><a href="{{url('/view-user')}}"><i class="fa fa-circle-o"></i> View User</a></li>
					</ul>
				</li>
				<li class="treeview">
					<a href="#"><i class="fa fa-circle-o"></i> Manage Permission Role
						<span class="pull-right-container">
							<i class="fa fa-angle-left pull-right"></i>
						</span>
					</a>
					<ul class="treeview-menu">
						<li><a href="{{url('/add-role')}}"><i class="fa fa-circle-o"></i> Add Role</a></li>
						<li><a href="{{url('/view-role')}}"><i class="fa fa-circle-o"></i> View Role</a></li>
					</ul>
				</li>
			</ul>
		</li> --->
        <li class="header">LABELS</li>
      </ul>
   </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Cont                                                                                                                                                                                                        ent Wrapper. Contai                                                                                                                                                                                                        ns page content -->
            <div class="content-wrapper">
                <!-- Main content -->
                @yield('content')
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    
                </div>
                <strong>Copyright � 2019 <a href="#">mcq</a>.</strong> All rights
                reserved.
            </footer>           
            <div class="control-sidebar-bg"></div>
        </div>

        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
        <script>
$.widget.bridge('uibutton', $.ui.button);</script>
        <script src="{{asset('asset_admin/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!--        <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>-->
        <script src="{{asset('asset_admin/plugins/select2/select2.full.min.js')}}"></script>
        <!-- InputMask -->
        <script src="{{asset('asset_admin/plugins/input-mask/jquery.inputmask.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="{{asset('asset_admin/plugins/morris/morris.min.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/knob/jquery.knob.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
        <script src="{{asset('asset_admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/fastclick/fastclick.js')}}"></script>
        <script src="{{asset('asset_admin/dist/js/adminlte.min.js')}}"></script>
        <script src="{{asset('asset_admin/dist/js/fileinput.min.js')}}"></script>
        <script src="{{asset('asset_admin/dist/js/pages/dashboard.js')}}"></script>
        <script src="{{asset('asset_admin/plugins/chartjs/Chart.min.js')}}"></script>
        <script src="{{asset('asset_admin/dist/js/pages/dashboard2.js')}}"></script>
        <script src="{{asset('asset_admin/dist/js/demo.js')}}"></script>
        <script src="{{asset('asset_admin/dist/js/form_validation.js')}}"></script>
        <script src="{{asset('assets/add_product.js')}}"></script>
        <script src="{{asset('asset_admin/ajax_modal.js')}}"></script>
        <script>
$(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
});
        </script>
        <script>
            $(function () {
                //Initialize Select2 Elements
                $(".select2").select2();
                //Datemask dd/mm/yyyy
                $("#datemask").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                //Datemask2 mm/dd/yyyy
                $("#datemask2").inputmask("mm/dd/yyyy", {"placeholder": "mm/dd/yyyy"});
                //Money Euro
                $("[data-mask]").inputmask();

                //Date range picker
                $('#reservation').daterangepicker();
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A'});
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                        {
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                            startDate: moment().subtract(29, 'days'),
                            endDate: moment()
                        },
                        function (start, end) {
                            $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                        }
                );

                //Date picker
                $('#datepicker').datepicker({
                    autoclose: true
                });

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass: 'iradio_minimal-blue'
                });
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red'
                });
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                });

                //Colorpicker
                $(".my-colorpicker1").colorpicker();
                //color picker with addon
                $(".my-colorpicker2").colorpicker();

                //Timepicker
                $(".timepicker").timepicker({
                    showInputs: false
                });
            });
        </script>
    </body>
</html>
