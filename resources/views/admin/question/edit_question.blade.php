@extends('admin.layouts.app')
@section('title', 'Edit Question')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <h1>
          Question
          <small>Edit</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li>Question</li>
          <li class="active">Edit</li>
        </ol>
</section>
<section class="container">
  <div class="row">
  <form class="form-horizontal" name="add_product" id="myform" action="{{url('/admin/update-question')}}" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
    <div class="col-md-12">
	 <div class="panel panel-default">
	  <div class="panel-heading">Edit Question</div>
		<section class="content">
			<!-- SELECT2 EXAMPLE -->
			<div class="panel-body">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
                            <center>
                                    @if (session('save'))
                                    <div class="alert alert-success">
                                        {{ session('save') }}
                                    </div>
                                    @endif
                                </center>
                                <center>
                                    @if (session('error'))
                                    <div class="alert alert-success">
                                        {{ session('error') }}
                                    </div>
                                    @endif
                                </center>
							<div class="form-group">
									<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Target Category</label>
									<div class="col-md-6">
										<select name="target" class="form-control" id="target_id">
												<option value=""> --- Select Target ---</option>
											<?php foreach($target_list as $target){ ?>
												<option value="<?php echo $target->id;?>"><?php echo $target->target_name;?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Category</label>
									<div class="col-md-6">
											<select name="category_id" class="form-control" id="category_id">
                                                    <option value=""> --- Select Category ---</option>
                                                    <?php foreach($category_list as $category){ ?>
                                                        <option value="<?php echo $category->id;?>"><?php echo $category->category_name;?></option>
                                                    <?php } ?>
											</select>
									</div>
								</div>
								<div class="form-group">
									<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Subject</label>
									<div class="col-md-6">
										<select name="subject_id" class="form-control" id="subject_id" required>
                                                <option value=""> --- Select Subject ---</option>
                                                <option value=""> --- Select Category ---</option>
                                                <?php foreach($subject_list as $subject){ ?>
                                                    <option value="<?php echo $subject->id;?>"><?php echo $subject->subject_name;?></option>
                                                <?php } ?>
										</select>
									</div>
								</div>
							<div class="clone_grp">
								<div class="product_style">
									<div id="right_part">
										<div class="formcontainer">
										<hr>
											<div class="form-group col-md-12">
												@if ($errors->any())
														{{ implode('', $errors->all('<div>:message</div>')) }}
												@endif												
											</div>
											<div class="form-group">
                                                  <label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Question</label>
												<div class="col-md-6">
                                                    <input type="hidden" name="id" class="form-control"  placeholder="Question" value="<?php echo $question_list->id; ?>" required />
                                                    <input type="text" name="question_text" class="form-control"  placeholder="Question" value="<?php echo $question_list->question_text; ?>" required />
												</div>
											</div>
											<div class="form-group">
												  <label  class="col-md-4 control-label clone_field">Answer Option 1</label>
												<div class="col-md-6">
													<input type="text"   name="option_one" class="form-control"  placeholder="Answer Option 1" value="<?php echo $question_list->option_one; ?>" required />
												</div>
											</div>
											<div class="form-group">
												 <label   class="col-md-4 control-label clone_field">Answer Option 2</label>
												<div class="col-md-6">
													<input type="text"    name="option_two" class="form-control"  placeholder="Answer Option 2" value="<?php echo $question_list->option_two; ?>" required />
												</div>
											</div>
											<div class="form-group">
												 <label  rel="input_option_3"  for="input_option_3" class="col-md-4 control-label clone_field">Answer Option 3</label>
												<div class="col-md-6">
													<input type="text"  rel="input_option_3"  name="option_three" class="form-control"  placeholder="Answer Option 3" value="<?php echo $question_list->option_three; ?>" required />
												</div>
											</div>
											<div class="form-group">
												 <label  rel="input_option_4"  for="input_option_4" class="col-md-4 control-label clone_field">Answer Option 4</label>
												<div class="col-md-6">
													<input type="text"  rel="input_option_4"  name="option_four" class="form-control"  placeholder="Answer Option 4" value="<?php echo $question_list->option_four; ?>" required />
												</div>
											</div>
											<div class="form-group">
												 <label  rel="input_crrect"  for="input_crrect" class="col-md-4 control-label clone_field">Correct Answer</label>
												<div class="col-md-6">
                                                    <input   type="radio"   name="currect_ans" value="1" <?php if($question_list->option_one == $question_list->currect_ans){ echo 'checked';}else{ echo '';} ?> > Option 1 
                                                    <input   type="radio" name="currect_ans" value="2" <?php if($question_list->option_two == $question_list->currect_ans){ echo 'checked';}else{ echo '';} ?> > Option 2 
                                                    <input   type="radio" name="currect_ans" value="3" <?php if($question_list->option_three == $question_list->currect_ans){ echo 'checked';}else{ echo '';} ?> > Option 3 
                                                    <input   type="radio" name="currect_ans" value="4" <?php if($question_list->option_four == $question_list->currect_ans){ echo 'checked';}else{ echo '';} ?> > Option 4 
													<!-- <input type="text"  rel="input_crrect"  name="input_crrect" class="txtfield clone_field form-control"  placeholder="Correct Answer" required /> -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="dynamic">
							</div>
						</div>
						<hr>
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<button type="submit" name="btnsubmit" class="submitbtn btn btn-primary pull-right">Update Question</button>
				</div>
			  </div>
		    </section>
	      </form>
		</div>
	</div>
   </div>
</section>
<script>
        document.getElementById("target_id").value = "<?php echo $question_list->target_id; ?>";
        document.getElementById("category_id").value = "<?php echo $question_list->category_id; ?>";
        document.getElementById("subject_id").value = "<?php echo $question_list->subject_id; ?>";
</script>
<script>
$( document ).ready(function() {
	$("#target_id").on('change',function(){
		var target_id=$("#target_id").val();
		var url=base_url + "/get-categoryby-target/"+target_id;
		$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: '',
				success: function (data)
				{
					$('#category_id').empty();
					$('#category_id').append('<option value=""> -- Select Category -- </option>');
					$.each(data, function (index, cat_obj) {
						$('#category_id').append('<option value="' + cat_obj.id + '">' + cat_obj.category_name + '</option>');
					});
					$("#category_id").on('change',function(){
						var category_id=$("#category_id").val();
		                var url=base_url + "/get-subjectby-category/"+category_id;
						$.ajax({
                             url:url,
							 type:'GET',
							 datatype:'json',
							 data: '',
							 success:function(data){
								 $("#subject_id").empty();
								 $("#subject_id").append("<option value=''> -- Select Subject -- </option>");
								 var subj_list=jQuery.parseJSON(data);
                                 $.each(subj_list, function (index, subject_obj) {
									$('#subject_id').append('<option value="' + subject_obj.id + '">' + subject_obj.subject_name + '</option>');
								 });
							 }
						});
					});
				}
			});
	});
	function checkDelete() {
		var checkstr = confirm('Are you sure you want to delete this?');
		if (checkstr == true) {
			return true;
		} else {
			return false;
		}
	}
});
</script>
@endsection