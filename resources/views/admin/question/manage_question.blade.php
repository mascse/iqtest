@extends('admin.layouts.app')
@section('title', 'Manage Question')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <h1>
          Question
          <small>Manage</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li>Question</li>
          <li class="active">Manage</li>
        </ol>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-12">
	 <div class="panel panel-default">
	  <div class="panel-heading">Manage Question</div>
		<section class="content">
			<!-- SELECT2 EXAMPLE -->
			<div class="panel-body">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
                        <center>
                                @if (session('save'))
                                <div class="alert alert-success">
                                    {{ session('save') }}
                                </div>
                                @endif
                            </center>
                            <center>
                                @if (session('error'))
                                <div class="alert alert-danger">
                                    {{ session('error') }}
                                </div>
                                @endif
                            </center>
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th width="5%" style="text-align:center;">SL</th>
                                        <th>Subject</th>
                                        <th>Question</th> 
                                        <th>Option One</th> 
                                        <th>Option Two</th> 
                                        <th>Option Three</th> 
                                        <th>Option Four</th> 
                                        <th>Correct Answer</th> 
                                        <th>Publication Status</th>
                                        <th style="text-align:center;"> 
                                            Action
                                        </th>                                    
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=0;  foreach($question_list as $question){ $i++; ?>
                                     <tr>
                                        <td><?php echo $i;?></td>
                                        <td><?php echo $question->subject_name;?></td>
                                        <td><?php echo $question->question_text;?></td>
                                        <td><?php echo $question->option_one;?></td>
                                        <td><?php echo $question->option_two;?></td>
                                        <td><?php echo $question->option_three;?></td>
                                        <td><?php echo $question->option_four;?></td>
                                        <td><?php echo $question->currect_ans;?></td>
                                        <?php if($question->block_status == 0){ ?>
                                        <td width="2%" style="text-align:center;">
                                            <a href='{{url("/admin/question/unpublished/{$question->id}")}}' class="btn btn-success btn-sm" title="not published"> <i class="fa fa-arrow-circle-down"></i></a>
                                        </td>
                                       <?php    }else{ ?>
                                        <td>
                                                <a href='{{url("/admin/question/published/{$question->id}")}}' class="btn btn-danger btn-sm" title="published"> <i class="fa fa-arrow-circle-up"></i></a>
                                        </td>
                                        <?php   } ?>
                                        <td style="text-align:center;">
                                           <a href='{{url("/admin/edit-question/{$question->id}")}}' class="btn btn-success btn-sm"> <i class="fa fa-edit (alias)"></i></a>
                                           <a  onclick="return checkDelete()" href='{{url("/admin/delete-question/{$question->id}")}}' class="btn btn-danger btn-sm"><i class="fa fa-close (alias)"></i></a>
                                        </td>
                                    </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
						</div>
						<hr>
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					
				</div>
			  </div>
		    </section>
	      </form>
		</div>
	</div>
   </div>
</section>
<script>
	function checkDelete() {
		var checkstr = confirm('Are you sure you want to delete this?');
		if (checkstr == true) {
			return true;
		} else {
			return false;
		}
	}
</script>
@endsection