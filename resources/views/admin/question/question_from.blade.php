@extends('admin.layouts.app')
@section('title', 'Add Question')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
        <h1>
          Question
          <small>Add</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li>Question</li>
          <li class="active">Add</li>
        </ol>
</section>
<section class="container">
  <div class="row">
  <form class="form-horizontal" name="add_product" id="myform" action="{{url('/admin/save-question')}}" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
    <div class="col-md-12">
	 <div class="panel panel-default">
	  <div class="panel-heading">Add Question</div>
		<section class="content">
			<!-- SELECT2 EXAMPLE -->
			<div class="panel-body">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
									<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Target Category</label>
									<div class="col-md-6">
										<select name="target" class="form-control" id="target_id">
												<option value=""> --- Select Target ---</option>
											<?php foreach($target_list as $target){ ?>
												<option value="<?php echo $target->id;?>"><?php echo $target->target_name;?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Category</label>
									<div class="col-md-6">
											<select name="category_id" class="form-control" id="category_id">
												<option value=""> --- Select Category ---</option>
											</select>
									</div>
								</div>
								<div class="form-group">
									<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Subject</label>
									<div class="col-md-6">
										<select name="subject_id" class="form-control" id="subject_id" required>
												<option value=""> --- Select Subject ---</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Set Code</label>
									<div class="col-md-6">
										<input type="text" name="set_code" class="form-control"/>
									</div>
								</div>
							<a class="add_style submitbtn btn btn-primary" href="#">Add More</a>
							<input type="hidden"  name="total_grp" id="myform" class="total_grp" value="">
							<div class="clone_grp">
								<div class="product_style">
									<div id="right_part">
										<div class="formcontainer">
										<hr>
										<center>
											@if (session('save'))
											<div class="alert alert-success">
												{{ session('save') }}
											</div>
											@endif
										</center>
										<center>
											@if (session('error'))
											<div class="alert alert-success">
												{{ session('error') }}
											</div>
											@endif
										</center>
											<div class="form-group col-md-12">
												@if ($errors->any())
														{{ implode('', $errors->all('<div>:message</div>')) }}
												@endif
												
												<label rel="blank"  for="blank">Add Question&nbsp;&nbsp;&nbsp;<small rel="blank" for="blank" class="label pull-right bg-green sl" style="color:black;font-size:100%;"></small>
												</label>&nbsp;&nbsp;&nbsp;Or&nbsp;&nbsp;&nbsp;<a rel="blank" for="blank" class="remove_style" href="#">Remove</a> 
											</div>
											<div class="form-group">
                                                  <label  rel="input_question"  for="input_question" class="col-md-4 control-label clone_field">Question</label>
												<div class="col-md-6">
                                                    <input type="text"  rel="input_question"  name="input_question" class="txtfield clone_field form-control"  placeholder="Question" required />
												</div>
											</div>
											<div class="form-group">
												  <label  rel="input_option_1"  for="input_option_1" class="col-md-4 control-label clone_field">Answer Option 1</label>
												<div class="col-md-6">
													<input type="text"  rel="input_option_1"  name="input_option_1" class="txtfield clone_field form-control"  placeholder="Answer Option 1" required />
												</div>
											</div>
											<div class="form-group">
												 <label  rel="input_option_2"  for="input_option_2" class="col-md-4 control-label clone_field">Answer Option 2</label>
												<div class="col-md-6">
													<input type="text"  rel="input_option_2"  name="input_option_2" class="txtfield clone_field form-control"  placeholder="Answer Option 2" required />
												</div>
											</div>
											<div class="form-group">
												 <label  rel="input_option_3"  for="input_option_3" class="col-md-4 control-label clone_field">Answer Option 3</label>
												<div class="col-md-6">
													<input type="text"  rel="input_option_3"  name="input_option_3" class="txtfield clone_field form-control"  placeholder="Answer Option 3" required />
												</div>
											</div>
											<div class="form-group">
												 <label  rel="input_option_4"  for="input_option_4" class="col-md-4 control-label clone_field">Answer Option 4</label>
												<div class="col-md-6">
													<input type="text"  rel="input_option_4"  name="input_option_4" class="txtfield clone_field form-control"  placeholder="Answer Option 4" required />
												</div>
											</div>
											<div class="form-group">
												 <label  rel="input_crrect"  for="input_crrect" class="col-md-4 control-label clone_field">Correct Answer</label>
												<div class="col-md-6">
												    <input type="radio"  rel="input_crrect"  class="txtfield clone_field" name="input_crrect" value="1"> Option 1 <input rel="input_crrect"  class="txtfield clone_field" type="radio" name="input_crrect" value="2"> Option 2 <input rel="input_crrect"  class="txtfield clone_field" type="radio" name="input_crrect" value="3"> Option 3 <input rel="input_crrect"  class="txtfield clone_field" type="radio" name="input_crrect" value="4"> Option 4 
													<!-- <input type="text"  rel="input_crrect"  name="input_crrect" class="txtfield clone_field form-control"  placeholder="Correct Answer" required /> -->
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="dynamic">
							</div>
						</div>
						<hr>
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<button type="submit" name="btnsubmit" class="submitbtn btn btn-primary pull-right">Save New Question</button>
				</div>
			  </div>
		    </section>
	      </form>
		</div>
	</div>
   </div>
</section>
<script>
$( document ).ready(function() {
	$("#target_id").on('change',function(){
		var target_id=$("#target_id").val();
		var url=base_url + "/get-categoryby-target/"+target_id;
		$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: '',
				success: function (data)
				{
					$('#category_id').empty();
					$('#category_id').append('<option value=""> -- Select Category -- </option>');
					$.each(data, function (index, cat_obj) {
						$('#category_id').append('<option value="' + cat_obj.id + '">' + cat_obj.category_name + '</option>');
					});
					$("#category_id").on('change',function(){
						var category_id=$("#category_id").val();
		                var url=base_url + "/get-subjectby-category/"+category_id;
						$.ajax({
                             url:url,
							 type:'GET',
							 datatype:'json',
							 data: '',
							 success:function(data){
								 $("#subject_id").empty();
								 $("#subject_id").append("<option value=''> -- Select Subject -- </option>");
								 var subj_list=jQuery.parseJSON(data);
                                 $.each(subj_list, function (index, subject_obj) {
									$('#subject_id').append('<option value="' + subject_obj.id + '">' + subject_obj.subject_name + '</option>');
								 });
							 }
						});
					});
				}
			});
	});
	function checkDelete() {
		var checkstr = confirm('Are you sure you want to delete this?');
		if (checkstr == true) {
			return true;
		} else {
			return false;
		}
	}
});
</script>
@endsection