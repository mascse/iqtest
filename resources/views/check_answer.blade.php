<?php 
  use App\Http\Controllers\ModelTestController;
  $user_id=Auth::user()->id;
  $total_correct_ans=ModelTestController::GetTotalAnswer($user_id,$question_type_id,$set_code);
?>
@extends('layouts.app')
@section('content')
<div class="container">
  <div class="row">
  <?php
        if(isset(Auth::user()->id)){ ?>
           <input type="hidden" name="user_id" id="userId" value="<?php echo Auth::user()->id;?>">
        <?php } ?>
         <p id="demo"></p>
  <form class="form-horizontal"  action="{{url('/model-test-submit')}}" method="post" enctype="multipart/form-data">
	{{ csrf_field() }}
    <div class="col-md-8 col-md-offset-2">
	 <div class="panel panel-default">
	  <div class="panel-heading">Your Model Test Result
        <h2>Your Score:
        <?php echo $total_correct_ans;?>
        </h2>
      </div>
		<section class="content">
			<!-- SELECT2 EXAMPLE -->
			<div class="panel-body">
				<!-- /.box-header -->
				<div class="box-body">
					<div class="row">
				     <div class="col-md-12">
					 <input type="hidden" name="question_type_id" value="<?php echo $question_type_id;?>" />
					 <input type="hidden" name="set_code" value="<?php echo $set_code;?>" />
                     <?php $i=0; foreach($iqinfos as $iqinfo){ $i++; 
                        
                        $check_ans=ModelTestController::CheckAnswer($iqinfo->id,$user_id,$question_type_id,$set_code);

                    ?>
                     <div class="w3-padding w3-light-grey">
                        <p class="w3-large" style="margin-bottom:10px;"><?php echo $i;?>. <?php echo $iqinfo->question_text;?></p>
                        <input type="hidden" name="starttime" value="">
						<input type="hidden" name="qid_<?php echo $i;?>" value="<?php echo $iqinfo->id;?>"/>
                        <input type="hidden" name="correct_answers_<?php echo $i;?>" value="<?php echo $iqinfo->currect_ans;?>" />
                        <div class="radio">
                          <label>
                             <input type="radio" name="user_submit_<?php echo $i;?>" id="<?php echo $i;?>" value="<?php echo $iqinfo->option_one;?>"> 
                             <?php echo $iqinfo->option_one;?>
                           </label>
                        </div>
                        <div class="radio">
                          <label>
                              <input type="radio" name="user_submit_<?php echo $i;?>" id="<?php echo $i;?>" value="<?php echo $iqinfo->option_two;?>"> 
                              <?php echo $iqinfo->option_two;?>
                            </label>
                          </div>
                          <div class="radio">
                           <label>
                             <input type="radio" name="user_submit_<?php echo $i;?>" id="<?php echo $i;?>" value="<?php echo $iqinfo->option_three;?>"> 
                             <?php echo $iqinfo->option_three;?>
                            </label>
                        </div>
                        <div class="radio">
                          <label>
                             <input type="radio" name="user_submit_<?php echo $i;?>" id="<?php echo $i;?>" value="<?php echo $iqinfo->option_four;?>"> 
                             <?php echo $iqinfo->option_four;?>
                           </label>
                        </div>
                        <br>
                       <?php 
                       $currect_ans=$check_ans->currect_ans;
                       if($currect_ans==1){
                        echo "<span style='color:green'>".$check_ans->user_ans."</span>";
                       }else{
                        echo "<span style='color:red'>".$check_ans->user_ans."</span>";
                       }
                       ?>
                    </div>
                     <?php } ?>
					 <input type="hidden" name="total_loop" value="<?php echo $i;?>" />
					 <input type="submit" class="w3-btn w3-orange w3-large w3-text-white" value=" Next "> 
						</div>
						<hr>
					</div>
					<!-- /.row -->
				</div>
				<div class="box-footer">
					<button type="submit" name="btnsubmit" class="submitbtn btn btn-primary pull-right">Submit Answer</button>
				</div>
			  </div>
		    </section>
	      </form>
		</div>
	</div>
   </div>
</div>
<script>
$( document ).ready(function() {
	$("#target_id").on('change',function(){
		var target_id=$("#target_id").val();
		var url=base_url + "/get-categoryby-target/"+target_id;
		$.ajax({
				url: url,
				type: 'GET',
				dataType: 'json',
				data: '',
				success: function (data)
				{
					$('#category_id').empty();
					$('#category_id').append('<option value=""> -- Select Category -- </option>');
					$.each(data, function (index, cat_obj) {
						$('#category_id').append('<option value="' + cat_obj.id + '">' + cat_obj.category_name + '</option>');
					});
					$("#category_id").on('change',function(){
						var category_id=$("#category_id").val();
		                var url=base_url + "/get-subjectby-category/"+category_id;
						$.ajax({
                             url:url,
							 type:'GET',
							 datatype:'json',
							 data: '',
							 success:function(data){
								 $("#subject_id").empty();
								 $("#subject_id").append("<option value=''> -- Select Subject -- </option>");
								 var subj_list=jQuery.parseJSON(data);
                                 $.each(subj_list, function (index, subject_obj) {
									$('#subject_id').append('<option value="' + subject_obj.id + '">' + subject_obj.subject_name + '</option>');
								 });
								$("#subject_id").on('change',function(){
									var subject_id=$("#subject_id").val();
									var url=base_url + "/get-subjectwise-set/"+subject_id;
									$.ajax({
										url:url,
										type:'GET',
										datatype:'json',
										data:'',
										success:function(data){
                                          //  alert(data);
										$("#set_code").empty();
										$("#set_code").append("<option value=''> -- Select Set Code -- </option>");
										var subj_list=jQuery.parseJSON(data);
										$.each(subj_list, function (index, subject_obj) {
											$('#set_code').append('<option value="' + subject_obj.set_code + '">' + subject_obj.set_code + '</option>');
										});
										}
									});
								});
							 }
						});
					});
				}
			});
	});
	function checkDelete() {
		var checkstr = confirm('Are you sure you want to delete this?');
		if (checkstr == true) {
			return true;
		} else {
			return false;
		}
	}
});
 </script>
<script src="{{ asset('assets/js/point.js') }}"></script>
@endsection