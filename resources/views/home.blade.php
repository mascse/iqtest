@extends('layouts.app')
@section('content')
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/home.css')}}" />
<div id="google_ads">
  <center>
   <img src="https://s0.2mdn.net/6241250/3dollarbundle-970x90.png" alt="Advertisement" border="0" width="970" height="90">
  </center>
</div>
<center><div id="block-status" style="color:red;"></div></center>
<div class="maincontent_area">
    <div class="container maincontent">
        <div class="row">
        <?php
        if(isset(Auth::user()->id)){ ?>
           <input type="hidden" name="user_id" id="userId" value="<?php echo Auth::user()->id;?>">
        <?php } ?>
         <p id="demo"></p>

            <div class="col-lg-2 col-md-2 col-sm-2">
                <div class="left_sidebar">
                    <div class="smart_img_top">
                        <div class="smart_img">
                            <?php
                            if(isset(Auth::user()->id)){ 
							$user_image=Auth::user()->avatar;
							$user_name=Auth::user()->name;
							$user_id=Auth::user()->id;
							}else{ 
							$user_image = '';
							$user_name='';
							$user_id=0;
							}
                            if ($user_image == null) {
                                ?>
                                <img src="{{url('/')}}/storage/app/public/mcq_logo.png" alt="Profile Photo" width="200px" height="233px" style="background-color: white;"/>
                            <?php } else { ?>
                                <img src="{{ Auth::user()->avatar }}" alt="Profile Photo" width="200px" height="233px"/>
                            <?php } ?>
                        </div>
                        <div class="smart_text" style="text-align: center;">
                            <p>
                                {{ $user_name}}
								<input type="hidden" id="user_id" value="<?php echo $user_id;?>"/>
                            </p>
                        </div> 								
                    </div>
                    <div class="left_menu">	
                        <ul id="nav">
                            <li><a href="{{url('/')}}"><img  class="menu_li" src="{{url('/')}}/storage/app/public/home.png" alt="" /><b>Home</b></a></li>
                            <li> <a href="#"><img  class="menu_li" src="{{url('/')}}/storage/app/public/instruction.png" alt="how to start contest" /><b>নির্দেশনা / সাহায্য</b></a></li>
                            @guest
                            <li><a href="{{ route('login') }}"><img  class="menu_li" src="{{url('/')}}/storage/app/public/log_in.png" alt="start education online" /><b>Log In</b></a></li>
                            <li><a href="{{ route('register') }}"><img  class="menu_li" src="{{url('/')}}/storage/app/public/registration.png" alt="registred member online" /><b>Registration</b></a></li>
                            @else
                            <li>
                                <a href="{{ route('user.logout') }}">
                                    <img  class="menu_li" src="{{url('/')}}/storage/app/public/registration.png" alt="registred member online" /><b>Logout</b>
                                </a>
                            </li>
                            @endguest
                        </ul>														
                    </div>
                    <div class="smart_img_top" style="margin-top:30px;">
                        <div class="smart_img">
                            <img src="" alt="" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-7 col-md-7 col-sm-7">
                <div class="middle_content">
                    <style type="text/css">
                        .style3 {font-weight: bold; color: #0000FF;}
                        .style6 {
                            color: #0033FF;
                            font-weight: bold;
                        }

                    </style>
                    <div id="loading">
                        <img class="loading-image" src="#" id="img" style="display:none"/>
                    </div>
                    <div class="middle_content_top">
                        <h2><a href="#">
                                <marquee id="test" behavior="scroll" direction="left" height="100%" scrolldelay="30" scrollamount="3" onMouseOver="document.all.test.stop()" onMouseOut="document.all.test.start()">Govt Jobs, BCS, Bank & Admission MCQ Question Practice & Exam Best Online Free Software in Bangladesh.</marquee></a></h2>
                    </div>
                    <form action="{{url('/reading')}}" method="get">
                        <div class="middle_content_middle_one">
                            <div class="panel panel-default">
                                <div class="panel-heading"><strong>MCQ পড়াশোনা শুরু করতে উপরের সব গুলো অপশন পূর্ণ করে "START" বাটনে ক্লিক করুন</strong></div>
                                <section class="content">
                                    <!-- SELECT2 EXAMPLE -->
                                    <div class="panel-body">
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label  rel="input_question"  for="input_question" class="col-md-12 control-label padding-top-5">Target Category</label>
                                                        <div class="col-md-12">
                                                            <select name="target" class="form-control" id="target_id" required>
                                                                <option value=""> --- Select Target ---</option>
                                                                <?php foreach ($target_list as $target) { ?>
                                                                    <option value="<?php echo $target->id; ?>"><?php echo $target->target_name; ?></option>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label  rel="input_question"  for="input_question" class="col-md-12 control-label padding-top-5">Category</label>
                                                        <div class="col-md-12">
                                                            <select name="category_id" class="form-control" id="category_id" required>
                                                                <option value=""> --- Select Category ---</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label  rel="input_question"  for="input_question" class="col-md-4 control-label padding-top-5">Subject</label>
                                                        <div class="col-md-12">
                                                            <select name="subject_id" class="form-control" id="subject_id" required>
                                                                <option value=""> --- Select Subject ---</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label  rel="input_question"  for="input_question" class="col-md-4 control-label padding-top-5">Set Code</label>
                                                        <div class="col-md-12">
                                                            <select name="set_code" class="form-control" id="set_code" required>
                                                                <option value=""> --- Select Set Code ---</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>
                                        <div class="box-footer"  style=" padding-top: 10px; padding-right: 15px;">
                                            <button type="submit" name="btnsubmit" class="submitbtn btn btn-primary pull-right">START</button>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                    </form>
                    <div class="middle_content_middle_one">
                            <div class="panel panel-default">
                                <div class="panel-heading"><strong>Payment status table</strong></div>
                                <section class="content">
                                    <!-- SELECT2 EXAMPLE -->
                                    <div class="panel-body">
                                        <!-- /.box-header -->
                                        <div class="box-body">
                                        <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="5%" style="text-align:center;">SL</th>
                                                <th>Full Name</th>
                                                <th>Phone</th> 
                                                <th style="text-align:center;">Payment Status</th>                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            if(!empty($payment_status)){
                                                $i=0;  foreach($payment_status as $user){ $i++; ?>
                                                <tr>
                                                    <td><?php echo $i;?></td>
                                                    <td><?php echo $user->name;?></td>
                                                    <td><?php echo $user->phone_number;?></td>
                                                    <?php if($user->status == 1){ ?>
                                                    <td style="text-align:center;">
                                                        <span style="text-align:center;color:red;">Pending</span>
                                                    </td>
                                                <?php    }else{ ?>
                                                    <td style="text-align:center;">
                                                    <span style="text-align:center;color:green;">Completed</span>
                                                    </td>
                                                    <?php   } ?>
                                                </tr>
                                                <?php } 
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                        </div>   
                                    </div>
                                </section>
                            </div>
                        </div>
                    <div class="middle_maincontent">
                        <div align="center">
                        </div>
                        <div class="middle_fbc_like_box" style="text-align: center; margin-bottom: 5px; margin-left: 7px; margin-top: 1px;">
                            <!-- MCQ Contest 1 -->
                            <ins class="adsbygoogle"
                                 style="display:block"
                                 data-ad-client="ca-pub-0669558921236313"
                                 data-ad-slot="9836623319"
                                 data-ad-format="auto">
                            </ins>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 right_sidebar_ad">
                <div class="right_sidebar">

                    <div class="right_sidebar_one" style="margin-bottom: 15px !important;">
                        <script async="" src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <ins class="adsbygoogle" style="display:inline-block;width:261px;height:250px" data-ad-client="ca-pub-0669558921236313" data-ad-slot="6418825314" data-adsbygoogle-status="done"><ins id="aswift_4_expand" style="display:inline-table;border:none;height:250px;margin:0;padding:0;position:relative;visibility:visible;width:261px;background-color:transparent;"><ins id="aswift_4_anchor" style="display:block;border:none;height:250px;margin:0;padding:0;position:relative;visibility:visible;width:261px;background-color:transparent;"><iframe width="261" height="250" frameborder="0" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" onload="var i = this.id, s = window.google_iframe_oncopy, H = s & amp; & amp; s.handlers, h = H & amp; & amp; H[i], w = this.contentWindow, d; try{d = w.document} catch (e){}if (h & amp; & amp; d & amp; & amp; (!d.body || !d.body.firstChild)){if (h.call){setTimeout(h, 0)} else if (h.match){try{h = s.upd(h, i)} catch (e){}w.location.replace(h)}}" id="aswift_4" name="aswift_4" style="left:0;position:absolute;top:0;border:0px;width:261px;height:250px;"></iframe></ins></ins></ins>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                    <div class="right_sidebar_one" style="margin-bottom: 15px !important;">
					<?php if(isset(Auth::user()->id)){ ?>
                        <center>
                            <table width="261" border="1" style="text-align:left;">
                                <tbody><tr>
                                        <td colspan="4" style="background-color: #023D99; color: white; text-align:center;"><b>Total Earning Points</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td width="40"><div align="center">ID</div></td>
                                        <td width="9"><div align="center">:</div></td>
                                        <td width="223">&nbsp;&nbsp;&nbsp;MCQ#000-<?php echo $user_id;?></td>
                                    </tr>
                                    <tr>
                                        <td>Point</td>
                                        <td><div align="center">:</div></td>
                                        <td>
                                            <span style="float: left;">&nbsp;&nbsp;&nbsp;<?php echo $total_points;?></span>
                                            <span style="float: right;"><a href="#" data-toggle="modal" data-target="#myModal">Payment Request</a></span>		    
										</td>
                                    </tr>
                                </tbody></table>
                        </center>
					<?php } ?>
                    </div>

                    <!---<div class="right_sidebar_one" style="margin-bottom: 15px !important;">
                        <center>
                            <table width="261" border="1" style="text-align:left;">
                                <tbody><tr>
                                        <td colspan="4" style="background-color: #023D99; color: white; text-align:left;"><b>Lottery Winners L2 (40 to 99 Point)</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td width="40"><div align="center">ID</div></td>
                                        <td width="9"><div align="center">:</div></td>
                                        <td width="223"></td>
                                    </tr>
                                    <tr>
                                        <td>Point</td>
                                        <td><div align="center">:</div></td>
                                        <td>
                                            <span style="float: left;"></span>
                                            <span style="float: right;"><a href="#">Full List</a></span>			    </td>
                                    </tr>
                                </tbody>
							</table>
                        </center>
                    </div>

                    <div class="right_sidebar_one" style="margin-bottom: 15px !important;">
                        <center>
                            <table width="261" border="1" style="text-align:left;">
                                <tbody><tr>
                                        <td colspan="4" style="background-color: #023D99; color: white; text-align:left;"><b>Lottery Winners L3 (100 to Highest)</b></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td width="40"><div align="center">ID</div></td>
                                        <td width="9"><div align="center">:</div></td>
                                        <td width="223"></td>
                                    </tr>
                                    <tr>
                                        <td>Point</td>
                                        <td><div align="center">:</div></td>
                                        <td>
                                            <span style="float: left;"></span>
                                            <span style="float: right;"><a href="#">Full List</a></span>			    </td>
                                    </tr>
                                </tbody></table>
                        </center>
                    </div> --->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Payment Request</h4>
      </div>
      <div class="modal-body">
	  <p id="payment_success" style="color:green;"></p>
        <form action="{{url('/payment-request')}}" id="payment_form" method="get">
		{{ csrf_field() }}
		    <div class="form-group">
				<label>Payment Amount</label>
				<input type="hidden" class="form-control" id="request_user_id" name="" value="<?php echo isset(Auth::user()->id);?>" required/>
				<input type="number" class="form-control" id="request_amount" name="request_amount" max="<?php echo isset($total_points)/50;?>" required/>
			</div>
			<div class="form-group">
			<label>Comments</label>
			   <textarea type="text" class="form-control" id="request_comment" name="request_comment" rows="3"></textarea>
			</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancle</button>
		<button type="submit" name="btnsubmit" class="submitbtn btn btn-primary pull-right" value="Request" id="payment_submit">Request</button>
      </div>
	 </form>
    </div>

  </div>
</div>
<script>
    $(document).ready(function () {
		var form=$("#payment_form");
			$("#payment_submit").click(function(){
				var user_id=$("#request_user_id").val();
				var request_amount=$("#request_amount").val();
				var request_comment=$("#request_comment").val();
			$.ajax({
					type:"GET",
					url:"{{url('/')}}/payment-request/"+user_id+"/"+request_amount+"/"+request_comment,
					success: function(response){
						if(response=='success') {
							$("#request_user_id").val(" ");
							$("#request_amount").val(" ");
							$("#request_comment").val(" ");
							$("#payment_success").html("Your payment request success.");
						} 
					}
				});
			});
			$("#target_id").on('change', function () {
			var target_id = $("#target_id").val();
			var url = base_url + "/get-categoryby-target/" + target_id;
			$.ajax({
			url: url,
					type: 'GET',
					dataType: 'json',
					data: '',
					success: function (data)
					{
					$('#category_id').empty();
					$('#category_id').append('<option value=""> -- Select Category -- </option>');
					$.each(data, function (index, cat_obj) {
					$('#category_id').append('<option value="' + cat_obj.id + '">' + cat_obj.category_name + '</option>');
					});
					$("#category_id").on('change', function () {
					var category_id = $("#category_id").val();
					var url = base_url + "/get-subjectby-category/" + category_id;
					$.ajax({
					url: url,
							type: 'GET',
							datatype: 'json',
							data: '',
							success: function (data) {
							$("#subject_id").empty();
							$("#subject_id").append("<option value=''> -- Select Subject -- </option>");
							var subj_list = jQuery.parseJSON(data);
							$.each(subj_list, function (index, subject_obj) {
							$('#subject_id').append('<option value="' + subject_obj.id + '">' + subject_obj.subject_name + '</option>');
							});
							$("#subject_id").on('change',function(){
								var subject_id=$("#subject_id").val();
								var url=base_url + "/get-subjectwise-set/"+subject_id;
								$.ajax({
									url:url,
									type:'GET',
									datatype:'json',
									data:'',
									success:function(data){
									  //  alert(data);
									$("#set_code").empty();
									$("#set_code").append("<option value=''> -- Select Set Code -- </option>");
									var subj_list=jQuery.parseJSON(data);
									$.each(subj_list, function (index, subject_obj) {
										$('#set_code').append('<option value="' + subject_obj.set_code + '">' + subject_obj.set_code + '</option>');
									});								
									}
								});
							});
						 }
					   });
					  });
					}
				});
			});
			var count = 0;
			$("#google_ads").click(function(){
				count += 1;
				if (count == 5) {
				  $( "#google_ads" ).hide();
				  var user_id=$("#user_id").val();
				  if(user_id !=0){
					  $.ajax({
						type:"GET",
						url:"{{url('/')}}/user-deactive/"+user_id,
						success: function(response){
							if(response=='success') {
								$("#block-status").html("Opps..you are blocked.");
							} 
						}
					});
				  }
				  alert('5');
				}
			});
     });
</script>
<script>
 var user_id= $("#userId").val();
 if(user_id){
    var totalmin = 0; 
    var totalSavemin =0;
    var x = setInterval(function() {
    var now = new Date().getTime();
    var seconds = Math.floor((now % (1000 * 60)) / 1000);
    if(seconds==59){
        totalmin++;
        if(totalmin<=8){
            totalSavemin=1; 
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var url=base_url + "/save_point";
            $.ajax({
                url:url,
                type:'POST',
                datatype:'json',
                data:{user_id: user_id, minute:totalSavemin},
                success:function(data){
        
                }
            })
       }else{
            window.location.href = base_url + "/users/logout";
       }
    }
  document.getElementById("demo").innerHTML = seconds + "s ";
//   if (distance < 0) {
//     clearInterval(x);
//   }
  }, 1000);
 }
</script>
@endsection
