@extends('layouts.app')
@section('content')
<div class="container text-left">    
  <div class="row">
  <?php
  if(isset(Auth::user()->id)){ ?>
      <input type="hidden" name="user_id" id="userId" value="<?php echo Auth::user()->id;?>">
  <?php } ?>
    <p id="demo"></p>
    @foreach($targets as $target)
    <div class="col-md-4">
      <div class="panel panel-default" style="height: 350px; overflow: auto;">
        <div class="panel-heading" style="background-color:#666; color:#FFF">{{ $target->target_name }}</div>
        <div class="panel-body">
          @foreach($target->categories as $category)
          <p><a href="{{ route('model.test', [$target->id, $category->id]) }}">{{ $category->category_name }}</a></p>
          @endforeach
          <p></p></div>
      </div>
    </div>
    @endforeach
  </div>
</div>
<script src="{{ asset('assets/js/point.js') }}"></script>
@endsection