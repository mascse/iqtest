@extends('layouts.app')
@section('content')
<link  rel="stylesheet" type="text/css"  media="all" href="{{asset('assets/css/home.css')}}" />
  <style>
	.container {
		background:#F3F3F3;
      	
	}
	.container>div.col-question {
		background:#FFF;
		box-shadow:0 0 5px #DEDEDE;
      	min-height:100vh;
      	border:1px solid #f00;
	}
	article div.text-strong {
		font-size: 1.2em;
		color: #222;
		font-weight:700;
	}
	h2.question {
		font-size:18px;
		color:#000;
		font-weight:700;
	}
	.custom-btn, .custom-btn:active, .custom-btn:hover {
		background: #1BBC9B;
		border-color:#1BBC9B
	}
	.custom-color {
		color: #1BBC9B;
	}
	.answer_container {
		border:1px solid #DEDEDE;
		padding:0 20px 10px;
		margin:10px 40px;
		box-shadow:0 0 5px #DEDEDE;
	}
  </style>
<div class="container text-left">    
  <div class="row">
  <?php
        if(isset(Auth::user()->id)){ ?>
           <input type="hidden" name="user_id" id="userId" value="<?php echo Auth::user()->id;?>">
        <?php } ?>
         <p id="demo"></p>
    <div class="middle_content col-md-offset-2 col-md-9 col-sm-offset-2 col-sm-10">
      <div class="col-question">
      @php($i = isset($_GET['page'])?$_GET['page']*5-5+1*5-5+1:1)
      @foreach($questions as $question)
		<article class="">
          <h2 class="question">{{ $i }}. {{ $question->question_text }}</h2>
			<div class="text-strong" style="margin-top:30px">
				<p>
					<label for="">A. </label>
					<input type="radio" name="option{{ $i }}">
                  	<label for="">{{ $question->option_one }}</label>
				</p>
				<p>
					<label for="">B. </label>
					<input type="radio" name="option{{ $i }}">
					<label for="">{{ $question->option_two }}</label>
				</p>
				<p>
					<label for="">C. </label>
					<input type="radio" name="option{{ $i }}">
					<label for="">{{ $question->option_three }}</label>
				</p>
				<p>
					<label for="">D. </label>
					<input type="radio" name="option{{ $i }}">
					<label for="">{{ $question->option_four }}</label>
				</p>
			</div>

			<div class="text-center">
              	<a href="#" onClick="event.preventDefault()" class="btn btn-success btn-sm custom-btn" data-toggle="collapse" data-target="#answer-{{ $i }}">Answer & Solution</a>
				<!---<a href="#" class="btn btn-success btn-sm custom-btn">Discuss in Board</a>
				<a href="#" class="btn btn-danger  btn-sm"><span>Save for Later</span></a> ---->
			</div>

			<div class="col-sm-12 answer_container collapse" id="answer-{{ $i }}">
				<div class="">
					<h2 class="custom-color">Answer & Solution</h2>
				</div>
				<hr style="margin-top: 5px !important;margin-bottom: 5px !important;">
				<div style="margin-top: 5px !important;margin-bottom: 5px !important;">
				   <span class="custom-color">Answer:</span>
				   <strong> Option @if($question->currect_ans == $question->option_one) A @elseif($question->currect_ans == $question->option_two) B @elseif($question->currect_ans == $question->option_three) C @elseif($question->currect_ans == $question->option_four) D @endif</strong> 
				</div>
				<!--<div>No explanation is given for this question <a class="text-primary" href="#"> Let's Discuss on Board</a></div> -->
			</div>
		</article>
      @php($i++)
      @endforeach
        <div class="row"><div class="col-sm-12 text-center"><span>{{ $questions->links() }}</span></div></div>
      </div>
    </div>
  </div>
</div>
<script src="{{ asset('assets/js/point.js') }}"></script>
@endsection
