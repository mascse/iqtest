<?php

namespace App\Http\Controllers\Auth;

use Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\login as RequestLogin;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout','userlogout']);
    }
	
	public function login(RequestLogin $request) {
        $vaildation = Validator::make($request->all(), [
                    'email' => 'required',
                    'password' => 'required'
        ]);
        if ($vaildation->fails()) {
            return redirect()->back()->withErrors($vaildation)->withInput();
        } else {
            $user_login = ['email' => $request->email, 'password' => $request->password, 'user_ip' =>request()->ip()];
            if (Auth::attempt($user_login, true)) {
                return redirect()->intended('/');
            } else {
                return redirect()->back()->with('failed', 'Your email or password or ip address not match.')->withInput();
            }
        }
    }
	
	public function userlogout(){
		Auth::guard('web')->logout();
		return redirect('/login');
	}
}
