<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Socialite;
use App\User;
use Auth;

class SocialLoginController extends Controller {
  
    public function redirectToProvider($service)
    {
        return Socialite::driver($service)->redirect();
    }

    public function handleProviderCallback($service)
    {
        try {
            if($service == 'google'){
                $user = Socialite::driver($service)->stateless()->user();
            }else{
                $user = Socialite::driver($service)->user();
            }
        } catch (Exception $e) {
            return redirect("auth/$service");
        }
        if($this->isUserExistsAnother($user, $service)) {
            \Session::flash('message', 'This email already exists in our database. Please try another way to login.');
            \Session::flash('class', 'warning');
            return redirect('/login');
           // return view('auth.login');
        }
        
        $authUser = $this->findOrCreateUser($user, $service);

        Auth::login($authUser, true);

        //  return redirect()->route('home');
        \Session::flash('message', 'Your account has been created.');
        \Session::flash('class', 'success');
        return redirect()->intended('/');
    }

    private function findOrCreateUser($socialUser, $service)
    {
        $authUser = User::where('social_id', $socialUser->id)->first();

        if ($authUser){
            return $authUser;
        }
        $user= User::create([
            'name' => $socialUser->name,
            'email' => $socialUser->email,
            'social_id' => $socialUser->id,
            'avatar' => $socialUser->avatar,
            'service' => $service
        ]);
        return $user;
    }
    private function isUserExistsAnother($socialUser, $service) {
        $user = User::where('email', $socialUser->email)->where('service','!=',$service)->first();
        return $user;
    }
}
