<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\MessageBag;

class AdminLoginController extends Controller
{
	public function __construct(){
		$this->middleware('guest:admin',['except'=>['logout']]);
	}
	public function ShowLoginForm(){
		return view('auth.admin.login');
	}
	
	public function Login(Request $request){
		$this->validate($request,[
		  'email'=>'required|email',
		  'password'=>'required'
		]);
		
		if(Auth::guard('admin')->attempt(['email'=>$request->email,'password'=>$request->password],$request->remember)){
			return redirect()->intended(route('admin.dashboard'));
		}else{
			$errors=new MessageBag(['email'=>['These credentials do not match our records.']]);
			return redirect()->back()->withErrors($errors)->withInput($request->only('email','remember'));
		}
	}
	
	public function logout(){
		Auth::guard('admin')->logout();
		return redirect('/admin');
	}
}
