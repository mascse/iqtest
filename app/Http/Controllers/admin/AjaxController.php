<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Illuminate\Support\Facades\Input;
use Response;
use Auth;

class AjaxController extends Controller
{
    public function GetCategoryByTarget($id){
        $category = DB::table('category')
                ->where('target_id', $id)
                ->where('status',1)
                ->get();
        return json_encode($category);

    }

    public function GetSubjectByCategory($category_id){
        $subject = DB::table('subject')
                ->where('category_id', $category_id)
                ->where('status',1)
                ->get();
        return json_encode($subject);
    }

    public function GetSetCodeBySubject($subject_id){
        $subject = DB::table('iqtestinfo')
                ->select('set_code')
                ->where('question_type_id', $subject_id)
                ->where('block_status',0)
                ->groupBY('set_code')
                ->get();
        return json_encode($subject);
    }

    public function AnswerSetCheck($id,$category_id,$subject_id,$set_code){
        $user_id =  Auth::user()->id;
        $count = DB::table('iqanswerinfo')
                ->where('question_type_id', $subject_id)
                ->where('set_code', $set_code)
                ->where('user_id',$user_id)
                ->count();
        return json_encode($count);
    }

    public function SaveMinutes(Request $request){
        $user_id =  Auth::user()->id;

        $point = $request->minute * 2;
        $point_info = DB::table('point_calculations')
            ->select('point')
            ->where('point_calculations.users_id','=', $user_id)
            ->first();

        if(!empty($point_info->point)){
            $data['users_id']=$user_id;
            $data['point']=$point + $point_info->point;
            DB::table('point_calculations')->update($data);
        }else{
            $data['users_id']=$user_id;
            $data['point']=$point;
            DB::table('point_calculations')->insert($data);
        }
    }
}
