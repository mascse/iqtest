<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;
use Carbon\Carbon;
use Auth;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function AddQuestionForm(){
		$data = array();
		$iqinfos = DB::select("select * from iqtestinfo");
		$data['target_list']=DB::table('target_category')->where('status',1)->get();
		$data['iqinfos'] =$iqinfos;
		return view('admin.question.question_from',$data);
    }
    
    public function SaveQuestion(Request $request){
        	//dd($request);
             $total_grp = $request->total_grp;
             for ($loopcountermain = 1; $loopcountermain <= $total_grp; $loopcountermain++) {
                 $txtquestionname="input_question_" . $loopcountermain;
                 $question=$request->$txtquestionname;
                 $txtquestionoption_1="input_option_1_" . $loopcountermain;
                 $option_1=$request->$txtquestionoption_1;
                 $txtquestionoption_2="input_option_2_" . $loopcountermain;
                 $option_2=$request->$txtquestionoption_2;
                 $txtquestionoption_3="input_option_3_" . $loopcountermain;
                 $option_3=$request->$txtquestionoption_3;
                 $txtquestionoption_4="input_option_4_" . $loopcountermain;
                 $option_4=$request->$txtquestionoption_4;
                 $txtcorrectanswer="input_crrect_" . $loopcountermain;
                 $correct_answer=$request->$txtcorrectanswer;
                 if($correct_answer == 1){
                     $correct=$option_1;
                 }elseif($correct_answer == 2){
                     $correct=$option_2;
                 }elseif($correct_answer == 3){
                     $correct=$option_3;
                 }elseif($correct_answer == 4){
                     $correct=$option_4;
                 }
                 $data['question_type_id']=$request->subject_id;
				 $data['set_code']=$request->set_code;
                 $data['question_text']=$question;
                 $data['option_one']=$option_1;
                 $data['option_two']=$option_2;
                 $data['option_three']=$option_3;
                 $data['option_four']=$option_4;
                 $data['currect_ans']=$correct;
				 $data['created_by']=Auth::user()->id;
                 $data['created_at']=Carbon::now();
                 DB::table('iqtestinfo')->insert($data);
             }
             return redirect()->back()->with('save', 'Question insert successfully!');
        }

    public function ManageQuestion(){
        $data['question_list']=DB::table('iqtestinfo')
        ->join('subject','iqtestinfo.question_type_id','=','subject.id')
        ->select('iqtestinfo.*','subject.subject_name')
        ->get();
        return view('admin.question.manage_question',$data);
    }

    public function EditQuestion($id){
        $data['target_list']=DB::table('target_category')->where('status',1)->get();
        $data['category_list']=DB::table('category')->where('status',1)->get();
        $data['subject_list']=DB::table('subject')->where('status',1)->get();
        $data['question_list']=DB::table('iqtestinfo')
        ->join('subject','iqtestinfo.question_type_id','=','subject.id')
        ->join('category','subject.category_id','=','category.id')
        ->join('target_category','subject.target_id','=','target_category.id')
        ->select('iqtestinfo.*','subject.subject_name','subject.id as subject_id','category.id as category_id','target_category.id as target_id')
        ->where('iqtestinfo.id',$id)
        ->first();
        return view('admin.question.edit_question',$data);
    }

    public function UpdateQuestion(Request $request){
        $correct_answer=$request->currect_ans;
        if($correct_answer == 1){
            $correct=$request->option_one;
        }elseif($correct_answer == 2){
            $correct=$request->option_two;
        }elseif($correct_answer == 3){
            $correct=$request->option_three;
        }elseif($correct_answer == 4){
            $correct=$request->option_four;
        }
        $data['question_type_id']=$request->subject_id;
        $data['question_text']=$request->question_text;
        $data['option_one']=$request->option_one;
        $data['option_two']=$request->option_two;
        $data['option_three']=$request->option_three;
        $data['option_four']=$request->option_four;
        $data['currect_ans']=$correct;
        $data['updated_at']=Carbon::now();
        DB::table('iqtestinfo')->where('id',$request->id)->update($data);
        return redirect()->back()->with('save', 'Question updated');
    }

    public function UnpublishedQuestion($question_id){
        $data['block_status']=1;
        DB::table('iqtestinfo')->where('id',$question_id)->update($data);
        return redirect()->back()->with('error', 'Question unpublished!');
    }

    public function PublishedQuestion($question_id){
        $data['block_status']=0;
        DB::table('iqtestinfo')->where('id',$question_id)->update($data);
        return redirect()->back()->with('save', 'Question published!');
    }

    public function DeleteQuestion($id){
        DB::table('iqtestinfo')->where('id',$id)->delete();
        return redirect()->back()->with('error', 'Question deleted');
    }
}
