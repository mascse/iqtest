<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function ManageUser(){
        $data['user_list']=DB::table('users')->where('block_status',0)->orderBy('id','DESC')->get();
        return view('admin.user.manage_user',$data);
    }

    public function UserBlock($id){
      $data['block_status']=1;
      DB::table('users')->where('id',$id)->update($data);
      return redirect()->back()->with('error', 'User blocked');
    }

    public function BlockUser(){
      $data['user_list']=DB::table('users')->where('block_status',1)->orderBy('id','DESC')->get();
      return view('admin.user.block_users',$data);
    }

    public function UserUnBlock($id){
      $data['block_status']=0;
      DB::table('users')->where('id',$id)->update($data);
      return redirect()->back()->with('save', 'User unblocked');
    }

    public function UserDelete($id){
      DB::table('users')->where('id',$id)->delete();
      return redirect()->back()->with('save', 'User deleted');
    }
}
