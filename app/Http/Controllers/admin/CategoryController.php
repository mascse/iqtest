<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    public function AddTarget(){
        $data['target_list']=DB::table('target_category')->where('status',1)->get();
        return view('admin.category.add_target',$data);
    }

    public function SaveTarget(Request $request){
        $vaildation = Validator::make($request->all(), [
            'target_name' => 'required|unique:target_category,target_name'
            ]);
            if ($vaildation->fails()) {
                return redirect()->back()->withErrors($vaildation)->withInput();
            } else {
                $data['target_name']=$request->target_name;
                DB::table('target_category')->insert($data);
                return redirect()->back()->with('save', 'Target added!');
            }
    }

    public function EditTarget($id){
        $data['target']=DB::table('target_category')->where('id',$id)->first();
        return view('admin.category.edit_target',$data);
    }

    public function UpdateTarget(Request $request){
        $vaildation = Validator::make($request->all(), [
            'target_name' => 'required|unique:target_category,target_name,'.$request->id,
            ]);
            if ($vaildation->fails()) {
                return redirect()->back()->withErrors($vaildation)->withInput();
            } else {
                $data['target_name']=$request->target_name;
                DB::table('target_category')->where('id',$request->id)->update($data);
                return redirect()->back()->with('save', 'Target updated!');
            }
    }

    public function DeleteTarget($id){
        DB::table('target_category')->where('id',$id)->delete();
        return redirect()->back()->with('error', 'Target deleted!');
    }

    public function AddCategory(){
        $data['category_list']=DB::table('category')
        ->select('category.*','target_category.target_name')
        ->join('target_category','category.target_id','=','target_category.id')
        ->where('category.status',1)
        ->get();
        $data['target_list']=DB::table('target_category')->where('status',1)->get();
        return view('admin.category.add_category',$data);
    }

    public function SaveCategory(Request $request){
        $vaildation = Validator::make($request->all(), [
            'category_name' => 'required|unique:category,category_name',
            'target_id' => 'required',
            ]);
            if ($vaildation->fails()) {
                return redirect()->back()->withErrors($vaildation)->withInput();
            } else {
                $data['target_id']=$request->target_id;
                $data['category_name']=$request->category_name;
                DB::table('category')->insert($data);
                return redirect()->back()->with('save', 'Category added!');
            }
    }

    public function EditCategory($id){
        $data['category_list']=DB::table('category')
        ->select('category.*','target_category.target_name')
        ->join('target_category','category.target_id','=','target_category.id')
        ->where('category.id',$id)
        ->first();
        $data['target_list']=DB::table('target_category')->where('status',1)->get();
        return view('admin.category.edit_category',$data);
    }

    public function UpdateCategory(Request $request){
        $vaildation = Validator::make($request->all(), [
            'category_name' => 'required|unique:category,category_name,'.$request->id,
            'target_id' => 'required',
            ]);
            if ($vaildation->fails()) {
                return redirect()->back()->withErrors($vaildation)->withInput();
            } else {
                $data['target_id']=$request->target_id;
                $data['category_name']=$request->category_name;
                DB::table('category')->where('id',$request->id)->update($data);
                return redirect()->back()->with('save', 'Category updated!');
            }
    }

    public function DeleteCategory($id){
        DB::table('category')->where('id',$id)->delete();
        return redirect()->back()->with('error', 'Category deleted!');
    }

    public function AddSubject(){
        $data['subject_list']=DB::table('subject')
        ->join('target_category','subject.target_id','=','target_category.id')
        ->join('category','subject.category_id','=','category.id')
        ->select('subject.*','target_category.target_name','category.category_name')
        ->where('subject.status',1)
        ->get();
        $data['target_list']=DB::table('target_category')->where('status',1)->get();
        return view('admin.category.add_subject',$data);
    }

    public function SaveSubject(Request $request){
        $vaildation = Validator::make($request->all(), [
            'target_id' => 'required',
            'category_id' => 'required',
            'subject_name' => 'required',
            ]);
            if ($vaildation->fails()) {
                return redirect()->back()->withErrors($vaildation)->withInput();
            } else {
               $data['target_id']=$request->target_id;
               $data['category_id']=$request->category_id;
               $data['subject_name']=$request->subject_name;
               DB::table('subject')->insert($data);
               return redirect()->back()->with('save', 'Subject added!');
            }
    }

    public function EditSubject($id){
        $data['subject_list']=DB::table('subject')
        ->join('target_category','subject.target_id','=','target_category.id')
        ->join('category','subject.category_id','=','category.id')
        ->select('subject.*','target_category.target_name','category.category_name')
        ->where('subject.id',$id)
        ->first();
        $data['category_list']=DB::table('category')->where('status',1)->get();
        $data['target_list']=DB::table('target_category')->where('status',1)->get();
        return view('admin.category.edit_subject',$data);
    }

    public function UpdateSubject(Request $request){
        $vaildation = Validator::make($request->all(), [
            'target_id' => 'required',
            'category_id' => 'required',
            'subject_name' => 'required',
            ]);
            if ($vaildation->fails()) {
                return redirect()->back()->withErrors($vaildation)->withInput();
            } else {
               $data['target_id']=$request->target_id;
               $data['category_id']=$request->category_id;
               $data['subject_name']=$request->subject_name;
               DB::table('subject')->where('id',$request->id)->update($data);
               return redirect()->back()->with('save', 'Subject updated!');
            } 
    }

    public function DeleteSubject($id){
        DB::table('subject')->where('id',$id)->delete();
        return redirect()->back()->with('error', 'Subject deleted!');
    }
}
