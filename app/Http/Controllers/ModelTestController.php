<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB;
use Auth;
use App\IqTestInfo;
use App\IqAnswerInfo;
use Session;

class ModelTestController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:web');
	}

	public static function CheckAnswer($question_id,$user_id,$subject,$set_code){
		$iqinfos = DB::table("iqanswerinfo")
		->where('question_id',$question_id)
		->where('user_id',$user_id)
		->where('question_type_id',$subject)
		->where('set_code',$set_code)
		->first();
		return $iqinfos;
	}

	public static function GetTotalAnswer($user_id,$subject,$set_code){
		$user_id =  Auth::user()->id;
		$data = [];
		$point = 0;
		$totalcorrectans = 0;
	    $totalcorrecinfos = DB::table("iqanswerinfo")
		->where('user_id',$user_id)
		->where('question_type_id',$subject)
		->where('set_code',$set_code)
		->where('user_id',$user_id)
		->get();
		$total_question=count($totalcorrecinfos);
       foreach($totalcorrecinfos as $check_ans){
		$currect_ans=$check_ans->currect_ans;
		if($currect_ans==1){
			$totalcorrectans++;
		}else{
		
		}
	  }
	  $data['user_id']=Auth::user()->id;
	  if($totalcorrectans==100){
        $point = $totalcorrectans*2+100;
	  }else if($totalcorrectans>=80 && $totalcorrectans<100){
		$point = $totalcorrectans*2+20;
	  }else{
		$point = $totalcorrectans*2; 
	  }
	  $data['point'] = $point;
	 // DB::table('pointinfo')->insert($data);
	  return $totalcorrectans;
	}
	
	public function index($target=null, $category=null){
		$data['target_list']=DB::table('target_category')->where('status',1)->get();
		$iqinfos = DB::table("iqtestinfo")->where('block_status',0)->get();
		$data['iqinfos'] =$iqinfos;
		$data['target_cat'] =$target;
		$data['cat'] =$category;
		return view('model_test',$data);
	}

	public function ModelTest()
	{
		$subject_id = Input::get('subject_id');
		$set_code = Input::get('set_code');
		Session::put('question_type_id', $subject_id);
		Session::put('set_code', $set_code);
		
		$iqinfos = IqTestInfo::where('question_type_id',$subject_id)
		->where('set_code',$set_code)
		->where('block_status',0)
		->inRandomOrder()
		->take(100)
		->get();
		$questions = array();
		Session::forget('questions');
        foreach($iqinfos as $single) {
            array_push($questions, array('id' => $single->id, 'answer' => -1));
        }
		
        if(!(Session::has('questions') && count(session('questions'))>0)) {
          Session::put('questions', $questions);
        }
		Session::put('active_question', 0);
		$this->validateActive();
		$data['iqinfo'] = IqTestInfo::find(session('questions')[session('active_question')]['id']);
		return view('model_question',$data);
	}
    
	public function nextOrPrevious(Request $request)
	{
		if(Session::has('questions')) {

          	if(isset($request->next) && $request->next == "Next") {
				$questions = session('questions');
				$questions[session('active_question')]['answer'] = $request->option;
				Session::put('questions', $questions);
          		Session::put('active_question', session('active_question')+1);
				
			} elseif(isset($request->previous) && $request->previous == "Previous") {
				Session::put('active_question', session('active_question')-1);
            }
        } else {
        	return redirect('model-test');
        }
		if(session('active_question')==count(session('questions'))) {
			return redirect('test-complete');
        }
		$this->validateActive();
		$data['iqinfo'] = IqTestInfo::find(session('questions')[session('active_question')]['id']);
		
		return view('model_question',$data);
	}
	public function testComplete() {

        foreach(session('questions') as $question) {
        $row = array('user_id' => Auth::user()->id, 'question_id' => $question['id'], 'question_type_id' => session('question_type_id'), 'set_code' => session('set_code'), 'user_ans' => $this->getUserAnswer($question['id'], $question['answer']), 'currect_ans' => $this->getCorrectAnswer($question['id']), 'wrong_ans' => '');
        IqAnswerInfo::create($row);
        }
        Session::forget('questions');
		
        return 'Congratulation';
    }
    public function cancelExam() {
        Session::forget('questions');
		return redirect('model-test');
    }
    private function validateActive() {
        if(session('active_question')<0)
            Session::put('active_question', 0);
        elseif(session('active_question')>=count(session('questions')))
            Session::put('active_question', count(session('questions'))-1);
		
    }
    private function getUserAnswer($id, $option) {
        $user_answer = '';
        $iqinfo = IqTestInfo::find($id);
        switch($option) {
          case 1:
          $user_answer = $iqinfo->option_one;
          break;
          case 2:
          $user_answer = $iqinfo->option_two;
          break;
          case 3:
          $user_answer = $iqinfo->option_three;
          break;
          case 4:
          $user_answer = $iqinfo->option_four;
          break;
          default:
          $user_answer = 'No answer';
        }
        return $user_answer;
    }
    private function getCorrectAnswer($id) {
        $iqinfo = IqTestInfo::find($id);
        return $iqinfo->currect_ans;
    }

	public function ModelTestSubmit(Request $request)
	{
		$total_grp=$request->total_loop;
	    for ($loopcountermain = 1; $loopcountermain <= $total_grp; $loopcountermain++) {
			$question_id="qid_" . $loopcountermain;
			$question=$request->$question_id;
			$correct_answer="correct_answers_" . $loopcountermain;
			$correct_ans=$request->$correct_answer;
			$user_answer="user_submit_" . $loopcountermain;
			$user_ans=$request->$user_answer;
			if($correct_ans == $user_ans){
				$data['currect_ans']=1;
				$data['wrong_ans']=0;
			}else{
				$data['wrong_ans']=1;
				$data['currect_ans']=0;
			}
			$data['user_ans']=$user_ans;
			$data['question_id']=$question;
			$data['question_type_id']=$request->question_type_id;
			$data['set_code']=$request->set_code;
			$data['user_id']=Auth::user()->id;
			$subject=$request->question_type_id;
			$set_code=$request->set_code;
			DB::table('iqanswerinfo')->insert($data);

          $user_id =  Auth::user()->id;
          $point = 0;
          $totalcorrectans = 0;
          $totalcorrecinfos = DB::table("iqanswerinfo")
          ->where('user_id',$user_id)
          ->where('question_type_id',$subject)
          ->where('set_code',$set_code)
          ->where('user_id',$user_id)
          ->get();
         foreach($totalcorrecinfos as $check_ans){
          $currect_ans=$check_ans->currect_ans;
          if($currect_ans==1){
              $totalcorrectans++;
          }else{
          
          }
        }
        $point_data['users_id']=Auth::user()->id;
        if($totalcorrectans==100){
          $point = $totalcorrectans*2;
          $bonus_point=100;
        }else if($totalcorrectans>=80 && $totalcorrectans<100){
          $point = $totalcorrectans*2+20;
          $bonus_point=20;
        }else{
          $point = $totalcorrectans*2; 
          $bonus_point=0;
        }
        $point_data['point'] = $point;
        $point_data['bonus_point'] = $bonus_point;
        DB::table('point_calculations')->insert($point_data);
		}
		return redirect("/check-answer/{$subject}/{$set_code}");
	}

	public function ModelTestCheckAnswer($subject_id,$set_code){
		$data['iqinfos'] = DB::table("iqtestinfo")
		->where('question_type_id',$subject_id)
		->where('set_code',$set_code)
		->where('block_status',0)
		->get();
		//dd($data);
		$data['question_type_id']=$subject_id;
		$data['set_code']=$set_code;
		return view('check_answer',$data);
	}
}
