<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\TargetCategory;
use Auth;

class HomeController extends Controller
{
    public function __construct()
    {
     //  $this->middleware('auth');
      $this->middleware('auth')->except(['index','reading','PrivacyPolicy','Home']);
    }

    public function index(){
       $data['target_list']=DB::table('target_category')->where('status',1)->get();
      
        return view('welcome',$data);
    }

    public function Home()
    {   
      //dd('ds');
	    if(isset(Auth::user()->id)){
	      $data['user_id']=Auth::user()->id;
		   $data['total_points']=DB::table('point_calculations')->where('users_id', Auth::user()->id)->sum(DB::raw('point + bonus_point'));
		}
	    $data['target_list']=DB::table('target_category')->where('status',1)->get();
        return view('home',$data);
    }

    public function PrivacyPolicy()
    {
       return view('privacy_policy');
    }
	public function selectExam()
    {
		$targets = TargetCategory::all();
       	return view('select_exam', compact('targets'));
    }
	
	public function reading(Request $request) {
	 $target_id = $request->target;
	 $category_id = $request->category_id;
	 $subject_id = $request->subject_id;
	 $set_code = $request->set_code;
   $questions = DB::table('iqtestinfo')->where('question_type_id',$subject_id)->where('set_code',$set_code)->paginate(20);
	return view('reading', compact('questions'));
  }
  
  public function PaymentRequest($id,$id2,$id3){
	  $data['user_id']=$id;
	  $data['amount']=$id2;
	  $data['comments']=$id3;
	  DB::table('payment_requests')->insert($data);
	  echo "success";
  }

  public function UserDeative($id){
    $data['block_status']=1;
    $result=DB::table('users')->where('id',$id)->update($data);
    if($result){
       toastr()->warning('Opps..you are blocked.');
       echo 'success';
    }
  }

}
