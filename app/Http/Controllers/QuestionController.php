<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Validator;
use DB;
use Carbon\Carbon;
use Auth;

class QuestionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:web');
	}

	public function AddQuestionForm(){

		$data = array();
		$iqinfos = DB::select("select * from iqtestinfo");
		$data['target_list']=DB::table('target_category')->where('status',1)->get();
		$data['iqinfos'] =$iqinfos;
		return view('question_from',$data);
	}
	
	public function SaveQuestion(Request $request){
	//	dd($request);
		 $total_grp = $request->total_grp;
		 for ($loopcountermain = 1; $loopcountermain <= $total_grp; $loopcountermain++) {
			 $txtquestionname="input_question_" . $loopcountermain;
			 $question=$request->$txtquestionname;
			 $txtquestionoption_1="input_option_1_" . $loopcountermain;
			 $option_1=$request->$txtquestionoption_1;
			 $txtquestionoption_2="input_option_2_" . $loopcountermain;
			 $option_2=$request->$txtquestionoption_2;
			 $txtquestionoption_3="input_option_3_" . $loopcountermain;
			 $option_3=$request->$txtquestionoption_3;
			 $txtquestionoption_4="input_option_4_" . $loopcountermain;
			 $option_4=$request->$txtquestionoption_4;
			 $txtcorrectanswer="input_crrect_" . $loopcountermain;
			 $correct_answer=$request->$txtcorrectanswer;
			 if($correct_answer == 1){
				 $correct=$option_1;
			 }elseif($correct_answer == 2){
				 $correct=$option_2;
			 }elseif($correct_answer == 3){
				 $correct=$option_3;
			 }elseif($correct_answer == 4){
				 $correct=$option_4;
			 }
			 $data['question_type_id']=$request->subject_id;
			 $data['question_text']=$question;
			 $data['option_one']=$option_1;
			 $data['option_two']=$option_2;
			 $data['option_three']=$option_3;
			 $data['option_four']=$option_4;
			 $data['currect_ans']=$correct;
			 $data['set_code']=$request->set_code;
			 $data['created_by']=Auth::user()->id;
			 $data['created_at']=Carbon::now();
			 DB::table('iqtestinfo')->insert($data);
		 }
		 return redirect()->back()->with('save', 'Question insert successfully!');
	}
	
  public function reading() {
  	$questions = DB::table('iqtestinfo')->paginate(5);
	return view('reading', compact('questions'));
  }
}
