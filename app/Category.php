<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

	public function targetCategory()
    {
        return $this->belongsTo('App\TargetCategory', 'target_id', 'id');
    }
}