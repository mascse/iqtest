<?php
namespace App;

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class TargetCategory extends Model
{
    protected $table = 'target_category';
	protected $primaryKey = 'id';

	public function categories()
    {
        return $this->hasMany('App\Category', 'target_id', 'id');
    }

    public function getList(){
        return DB::table('target_category')->get();
    }
}
