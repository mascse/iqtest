<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
	
	'google' => [
        'client_id' => '355700220623-e1kemka5cf7k85f9vmp3e3qiku03ijk3.apps.googleusercontent.com',
        'client_secret' => 'rYJdKffK0Bb9TwFzPkRpog5j',
        'redirect' => 'http://www.examtestbd.com/auth/google/callback',
    ],
    'facebook' => [
		'client_id' => '336694773843320',
		'client_secret' => 'e57b14831edf406684ee02ea85c02680',
		'redirect' => 'https://www.examtestbd.com/auth/facebook/callback',
	],


];
