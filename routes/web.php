<?php

Auth::routes();
Route::get('/', 'HomeController@Home')->name('index');
Route::get('/home', 'HomeController@Home')->name('home');
Route::get('/select-exam', 'HomeController@selectExam')->name('select.exam');
Route::get('/privacy-policy', 'HomeController@PrivacyPolicy')->name('PrivacyPolicy');
Route::get('/add-question', 'QuestionController@AddQuestionForm')->name('add.question');
Route::post('/save-question', 'QuestionController@SaveQuestion')->name('save.question');
Route::get('/users/logout','Auth\LoginController@userlogout')->name('user.logout');
//Route::get('/reading','HomeController@reading')->name('reading');
Route::get('/reading','HomeController@reading')->name('reading');
//payment
Route::get('/payment-request/{id}/{id2}/{id3}', 'HomeController@PaymentRequest')->name('PaymentRequest');
//model test
Route::get('/model-test/{target?}/{category?}', 'ModelTestController@index')->name('model.test');
Route::get('/model-test-set', 'ModelTestController@ModelTest')->name('model.test.set');
Route::post('/mt', 'ModelTestController@nextOrPrevious')->name('next.previous');
Route::get('/mt', 'ModelTestController@nextOrPrevious');
Route::get('/test-complete', 'ModelTestController@testComplete');
Route::post('/model-test-submit', 'ModelTestController@ModelTestSubmit')->name('model.test.submit');
Route::get('/check-answer/{id}/{id1}','ModelTestController@ModelTestCheckAnswer')->name('check.answer');
//user login
Route::post('/login', 'Auth\LoginController@login')->name('user.login');

//social login
Route::get('/privacy-cookies', 'HomeController@PrivacyPolicy');
Route::get('/user-deactive/{id}', 'HomeController@UserDeative');

Route::get('auth/{service}', 'Auth\SocialLoginController@redirectToProvider');
Route::get('auth/{service}/callback', 'Auth\SocialLoginController@handleProviderCallback');
//admin 
Route::prefix('admin')->group(function(){
   Route::get('/login','Auth\AdminLoginController@ShowLoginForm')->name('admin.login');
   Route::post('/login','Auth\AdminLoginController@Login')->name('admin.login.submit');
   Route::get('/', 'admin\AdminController@index')->name('admin.dashboard');	
   Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
   Route::POST('/password/email','Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
   Route::GET('/password/reset','Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request'); 
   Route::POST('/password/reset', 'Auth\AdminResetPasswordController@reset');
   Route::GET('/password/reset/{token}','Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
});
//category
Route::get('/add-target','admin\CategoryController@AddTarget')->name('add.target');
Route::post('/save-target','admin\CategoryController@SaveTarget')->name('save.target');
Route::get('/edit-target/{id}','admin\CategoryController@EditTarget')->name('edit.target');
Route::post('/update-target','admin\CategoryController@UpdateTarget')->name('update.target');
Route::get('/delete-target/{id}','admin\CategoryController@DeleteTarget')->name('delete.target');

Route::get('/add-category','admin\CategoryController@AddCategory')->name('add.category');
Route::post('/save-category','admin\CategoryController@SaveCategory')->name('save.category');
Route::get('/edit-category/{id}','admin\CategoryController@EditCategory')->name('edit.category');
Route::post('/update-category','admin\CategoryController@UpdateCategory')->name('update.category');
Route::post('/update-delete','admin\CategoryController@DeleteCategory')->name('delete.category');

Route::get('/add-subject','admin\CategoryController@AddSubject')->name('add.subject');
Route::post('/save-subject','admin\CategoryController@SaveSubject')->name('save.subject');
Route::get('/edit-subject/{id}','admin\CategoryController@EditSubject')->name('edit.subject');
Route::post('/update-subject','admin\CategoryController@UpdateSubject')->name('update.subject');
Route::get('/delete-subject/{id}','admin\CategoryController@DeleteSubject')->name('delete.subject');
//admin question add 
Route::get('/admin/add-question/','admin\QuestionController@AddQuestionForm')->name('admin.add.question');
Route::post('/admin/save-question', 'admin\QuestionController@SaveQuestion')->name('admin.save.question');
Route::get('/admin/manage-question', 'admin\QuestionController@ManageQuestion')->name('admin.manage.question');
Route::get('/admin/question/unpublished/{id}', 'admin\QuestionController@UnpublishedQuestion')->name('question.unpublished');
Route::get('/admin/question/published/{id}', 'admin\QuestionController@PublishedQuestion')->name('question.unpublished');
Route::get('/admin/edit-question/{id}', 'admin\QuestionController@EditQuestion')->name('admin.question.edit');
Route::post('/admin/update-question', 'admin\QuestionController@UpdateQuestion')->name('admin.question.update');
Route::get('/admin/delete-question/{id}', 'admin\QuestionController@DeleteQuestion')->name('admin.question.delete');
//admin user control

//
Route::get('/admin/payment_status/','admin\UserController@PaymentStatus')->name('admin.user.payment-status');
Route::get('/admin/manage-user/','admin\UserController@ManageUser')->name('admin.manage.user');
Route::get('/admin/block-user/','admin\UserController@BlockUser')->name('admin.manage.user.block');
Route::get('/admin/user-block/{id}','admin\UserController@UserBlock')->name('admin.block.user');
Route::get('/admin/user-unblock/{id}','admin\UserController@UserUnBlock')->name('admin.unblock.user');
Route::get('/admin/user-delete/{id}','admin\UserController@UserDelete')->name('admin.delete.user');
//admin.
//ajax call
Route::get('/get-categoryby-target/{id}','admin\AjaxController@GetCategoryByTarget')->name('get.category');
Route::get('/get-subjectby-category/{id}','admin\AjaxController@GetSubjectByCategory')->name('get.subject');
Route::get('/get-subjectwise-set/{id}','admin\AjaxController@GetSetCodeBySubject')->name('get.setcode');

Route::get('/answer_set_check/{id1}/{id2}/{id3}/{id4}','admin\AjaxController@AnswerSetCheck')->name('get.answercheck');
Route::post('/save_point','admin\AjaxController@SaveMinutes')->name('SaveMinutes');



