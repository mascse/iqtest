$(document).ready(function () {
    $("#ddlprocat").change(function () {
        var procat = $("#ddlprocat").val();
        // alert(procat)
        var url_op = base_url + "/subpro-list/" + procat;
        $.ajax({
            url: url_op,
            type: 'GET',
            dataType: 'json',
            data: '',
            success: function (data) {
                // alert(html);
                // $('#CityList').html(html);
                $('#ddlprosubcat1').empty();
                $('#ddlprosubcat1').append('<option value="">Select Sub Category</option>');
                $.each(data, function (index, supproobj) {
                    //   $('#ProductSizeList').append('<option value="' + subcatobj.productsize_id + '">' + subcatobj.productsize_size + '</option>');

                    $('#ddlprosubcat1').append('<option value="' + supproobj.subprocat_id + '">' + supproobj.subprocat_name + '</option>');
                });
            }
        });
    });
});
$(document).ready(
        function () {
            $('#isspecial').is(':checked');
            var grp = $('.clone_grp').clone(true);
            $('.add_style').click(function () {
                add_grp();
                return false;
            });
            add_grp();
            $('.remove_style').click(function () {

                remove(this);
                return false;
            });
            function chksize_click(obj) {
                var input = $(obj).attr('name');
                //alert('input[name=input_'+ input +']');
                if ($(obj).is(':checked')) {
                    $('input[name=input_' + input + ']').show();
                } else {
                    $('input[name=input_' + input + ']').hide();
                    $('input[name=input_' + input + ']').val("");
                }

            }
            ;
            function add_grp(obj) {
                $('.dynamic').append(grp.html());
                grp_arng();
                $('.remove_style').unbind("click");
                $('.remove_style').bind("click", function () {
                    remove(this);
                    return false;
                });
                $('.chksize').unbind("change");
                $('.chksize   ').bind("change", function () {
                    chksize_click(this);
                    return false;
                });
            }//add add_subgrp


            function remove(obj) {
                $(obj).parent().parent().parent().parent().fadeOut('slow',
                        function () {
                            $(obj).parent().parent().parent().parent().remove();
                            grp_arng();
                        }
                );
            }//remove

            function grp_arng() {
                var i = 0;
                $('.product_style').each(function () {
                    i++;
                    $(this).find('.sl').html(i);
                    $(this).find('.clone_field').each(function () {
                        var name = $(this).attr('rel');
                        //alert($(this).attr('for'));

                        if ($(this).attr('for') == null) {
                            $(this).attr('name', name + '_' + i);
                        } else {
                            $(this).attr('for', name + '_' + i);
                        }
                    });
                }); //each clone_field prod    uc    t_st    yle
                $('.total_grp').attr('value', i);
            }//grp_arng
            $('.clone_grp').remove();
            grp_arng();
        });


