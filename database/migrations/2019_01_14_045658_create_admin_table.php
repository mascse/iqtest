<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
            $table->string('password');
			$table->string('admin_type')->nullable();
			$table->dateTime('admin_lastlogin')->nullable();
			$table->string('admin_ip')->nullable();
			$table->tinyInteger('admin_status')->nullable();
			$table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}
